***********************
Technical Documentation
***********************

Developers
==========

.. warning:: This tool is still in development and many features are planned, before branching the code, please consider what you will miss out :p

.. note:: Main features on the todo list:

    * support for in-between shapes
    * support for cascading corrective shapes
    * auto animated shapes option
    * animation test clip

Module Details
==============

.. toctree::

   module/shared/handlers/index
   module/target_manager/index