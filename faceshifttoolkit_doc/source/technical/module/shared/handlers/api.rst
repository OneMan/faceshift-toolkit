faceshifttoolkit.shared.handlers.api
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: faceshifttoolkit.shared.handlers.api.Node
    :members:
    :show-inheritance:

.. autoclass:: faceshifttoolkit.shared.handlers.api.BlendShapeNode
    :members:
    :show-inheritance:
