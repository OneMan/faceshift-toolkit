.. faceshift toolkit documentation master file, created by
   sphinx-quickstart on Wed Oct 29 10:05:26 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###########################
faceshift toolkit |version|
###########################

.. important::
    The faceshift toolkit is free software:
    you can redistribute it and/or modify it under the terms of the GNU General Public License,
    either version 3 of the License, or (at your option) any later version.

**************
Target manager
**************

.. image:: ./images/app_screenshot.png
   :height: 800 px
   :width: 733 px
   :scale: 20 %
   :align: right


**Target Manager** is a blend shape target editing tool. It allows for easy addition, edition and renaming of blend shape targets.

:Author: Faceshift/Guillaume Barlier
:Repository: `https://bitbucket.org/faceshift/faceshift-toolkit`
:Date: |today|

.. note:: The **Target Manager** is provided with a working template for faceshift, it is however possible to load custom file containing a blendShape to use it as a custom template.

********
Contents
********

.. toctree::

   installation
   user/index
   technical/index


