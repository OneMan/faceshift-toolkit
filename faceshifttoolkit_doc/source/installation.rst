************
Installation
************

Dependencies
============

This module has the following dependencies:

* PySide or PyQt4 (PySide is provided by default with Maya 2015 or later)
* Maya 2015+ (should work on earlier versions with PySide or PyQt, but not tested, please report any issues)


How to run the tool
===================

To load the tool, following this few steps:

* Get the latest version from the repository
* Extract the package folder and put it in a folder present in your python path (the maya script folder is in the path by default)
* Run the following command in a python shell (in maya):

    .. code-block:: python

        from facesfhittoolkit import target_manager
        target_manager.load()

Troubleshooting
===============

* If you get an import error: "No module named facesfhittoolkit", that means that the parent folder is not in the python path.

    * To add your custom path to the python path, run the following commands in the python shell (replace "path_to_the_parent_folder" with the file path to you faceshift package).

        .. code-block:: python

            import sys
            path = "path_to_the_parent_folder"
            if not path in sys.path:
                sys.path.append(path)

    * Run the command from the "load steps" again

* For any other issue, you can contact Guillaume from Faceshift.

.. toctree::
   :maxdepth: 2
