User interface
==============

.. note:: Many options and actions are available in context menus, by right clicking the elements in the window.

Interface details
-----------------

The user interface is comprised of three sections:

* BlendShapes

    .. image:: ./images/blend_shape_group.png
       :height: 70 px
       :width: 315 px
       :scale: 100 %
       :align: center

    Here you can select the blendShape node you want to work on.

    Use the little refresh button at the right to refresh the list or the ui if needed.

* Templates

    .. image:: ./images/template_group.png
       :height: 69 px
       :width: 315 px
       :scale: 100 %
       :align: center

    The combobox element allows you to select the template you want to use as a target list reference.

    You will notice an "import..." option in the drop down menu, it allows you to use any maya file (ma, mb or fbx) as a template reference as long as it contains a blendShape node.

    .. note:: To use the selected template you need to tick the checkbox that is located at the left of the selector.

        Turning it on and off will import the template in reference or unload it.

* Targets

    .. image:: ./images/targets_group.png
       :height: 292 px
       :width:  315 px
       :scale: 100 %
       :align: center

    - This group contains a string filter at the top allowing you to filter your targets
    - The list right under it will list all the targets on the active blend shape node and their current status.
    - Their status is based on edition or if they are missing or valid based on the current active template (for status icons details see ferther down).

.. _status_icons_anchor:

Status icon details
-------------------

.. image:: ./images/light_green.png
       :height: 100 px
       :width:  100 px
       :scale: 25 %
       :align: left

**Valid**: Green represent a target from the template rig that is present on the active blendShape node.

.. image:: ./images/light_red.png
       :height: 100 px
       :width:  100 px
       :scale: 25 %
       :align: left

**Missing**: Red represent a target from the template rig that is missing from the active blendShape node.

.. image:: ./images/light_yellow.png
       :height: 100 px
       :width:  100 px
       :scale: 25 %
       :align: left

**Editing**: Yellow represent a target that is currently in edit mode (a mesh of the same name is present in the scene).

.. image:: ./images/light_grey.png
       :height: 100 px
       :width:  100 px
       :scale: 25 %
       :align: left

**Unknown**: Grey represent a target that exists on the blendShape but that the template does not recognise.

.. note:: Combination of icons (such as grey/yellow) exists, representing mixed status.

.. toctree::
   :maxdepth: 3
