******************
User documentation
******************

This section contains all the informations on how to use the tool and what steps to follow.

It details of the user interface and provides a walkthrough of the steps to follow in order to setup your scene and edit your targets.

.. toctree::

   interface
   walkthrough
   video_tutorials