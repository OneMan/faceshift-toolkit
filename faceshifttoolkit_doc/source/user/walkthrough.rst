Walkthrough
===========

Here is a quick walkthrough of the basic steps to follow to use a template reference and edit targets on a blendShape node.

Choosing a blendShape
---------------------

.. note:: You can either use an existing blendShape from the scene or choose to create a new one.

.. image:: ./images/blend_shape_group.png
           :width: 315 px
           :height: 70 px
           :scale: 75 %
           :align: right

*
    Select your blendShape from the list present in the scene.

    If you can not find it in the blendShape selection list, click the refresh button on the right side of the selector box.

.. image:: ./images/blend_create.png
           :width: 315 px
           :height: 69 px
           :scale: 75 %
           :align: right

*
    To create a new blendShape, select the mesh you want to work on then right click the blendShape selector and select the "Create..." option.

    It will bring up a window asking the name you want to give the new blendShape.



Using a Template
----------------

.. note::
    You have the choice to use the default faceshift template (available per build version) or reference your own custom blendShape to use as a template.

    Using a template will provide you usefull comparison imformation with your blendShape allowing you to see easily what shapes are missing of misnamed.

.. image:: ./images/template_selection.png
           :width: 315 px
           :height: 85 px
           :scale: 75 %
           :align: right

*
    Click on the template field to select the template you would like to use.

    by default you'll see the faceshift template

.. image:: ./images/template_import.png
           :width: 315 px
           :height: 85 px
           :scale: 75 %
           :align: right

*
    If you click on the "Import..." option it will open up a browse window for you the select the file you wish to use.

    Only maya ascii (*.ma), binary (*.ma) and fbx files are currently suported.



.. attention::

    .. image:: ./images/template_checkbox.png
           :width: 315 px
           :height: 85 px
           :scale: 75 %
           :align: right

    Remember to tick the check box next to the template field to toggle the use of the template.

    This will load or unload the template.



Target List Interactions
------------------------

.. note:: This list allows you to interact with the target, providing edition, create and renaming options.

Filtering targets
^^^^^^^^^^^^^^^^^

.. image:: ./images/target_filter.png
           :width: 315 px
           :height: 120 px
           :scale: 75 %
           :align: right

* You can filter the displayed shapes with the text field above the list
* By simply clicking the related column you can sort the list be name or index

Targets Options
^^^^^^^^^^^^^^^

* To Editing a target, follow this simple steps:

    .. image:: ./images/target_edit.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        You can either double click the target you wish to edit in the list, or right click et and select "Edit".

        Doing so will generate the target mesh.

    - Edit/Tweak the target mesh with the desired changes

    .. image:: ./images/target_apply.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        To apply the changed, simply double click the target in the list again or right click and select the 'Apply" option.

        This action, after applying the target offset to the blendShape data, will delete the target mesh, this is normal as it is not needed any more.

    .. note::

        You'll notice other options available in the context menu brought up by right clicking a target in the list.

        Depending on the current status of the target, the options will vary.

* Adding new shapes

    .. image:: ./images/target_missing.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        When using a template, you can simply add a missing (red status) target by double clicking it or right clicking and selecting the "Create" option.

        Then simply apply the changes to add the target to the blendShape.

        This will create a new shape with the offset on the blendShape node.

    .. image:: ./images/target_add.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        To manually add a custom shape simply press the "Add..." button located under the target list.

        This will open a window and ask you the desired name of the new target.

    .. note:: When creating a new target, the mesh will always be generated from the neutral shape before deformation, you don't have to worry disable deformations first.

* Renaming existing target

    .. image:: ./images/target_rename.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        Simply right click the target in the list and select the "Rename..." option, it will ask you for the new name.

        (This action will also rename the related target mesh if the renamed target is currently being edited.)

.. seealso:: For more details on target status icons color code, please see: :ref:`status_icons_anchor`.

* Driving target value

    .. image:: ./images/target_value_drag.png
               :width: 315 px
               :height: 70 px
               :scale: 75 %
               :align: right

    -
        Left click a target and drag your mouse right or left to drive the value of the target.

.. tip::
    You can activate several shapes together and check combination result by control clicking several targets.
    You can drive several targets at once together by control clicking them and keeping control pressed while dragging the mouse.

    By control clicking a single target you toggle it on and off, if you toggle them all off you'll get the neutral shape.

Batch Processing
^^^^^^^^^^^^^^^^

.. image:: ./images/edit_menu.png
       :width: 155 px
       :height: 70 px
       :scale: 100 %
       :align: right

You have access to a few batch processing features in the "Edit" menu.

* Generate all targets

    This option will generate all the target mesh, allowing you to save or export them to other software.

* Apply all targets

    This option will parse all the target, if a mesh matching the target name exist, it will apply it and upate the blendshape with it.

* Search and replace...

    This option will open a user input window allowing you to search a string (regular expression supported) and replace it with an other string in all the target names.

* Reorder targets

    This option will reorder alphabetically all the target on the active blend shape node.

.. toctree::

