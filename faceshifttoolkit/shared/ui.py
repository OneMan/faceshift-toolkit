# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

from faceshifttoolkit.shared.handlers.qt import QtCore
from faceshifttoolkit.shared.handlers.qt import QtGui


class CallbackButton(QtGui.QPushButton):
    """
    Dynamic callback button
    """
    def __init__(self, callback=None, tool_tip=None, *args, **kwargs):
        QtGui.QPushButton.__init__(self)
        self.callback = callback
        self.args = args
        self.kwargs = kwargs

        # Connect event
        self.connect(self, QtCore.SIGNAL("clicked()"), self.click_event)

        # Set tooltip
        if tool_tip:
            self.setToolTip(tool_tip)
        elif hasattr(self.callback, '__doc__') and self.callback.__doc__:
            self.setToolTip(self.callback.__doc__)

    def click_event(self):
        if not self.callback:
            return
        self.callback(*self.args, **self.kwargs)


class CallbackComboBox(QtGui.QComboBox):
    """
    Dynamic combo box object
    """
    def __init__(self, callback=None, status_tip=None, *args, **kwargs):
        QtGui.QComboBox.__init__(self)
        self.callback = callback
        self.args = args
        self.kwargs = kwargs
        if status_tip:
            self.setStatusTip(status_tip)

        self.connect(self, QtCore.SIGNAL('currentIndexChanged(int)'), self.index_change_event)

    def index_change_event(self, index):
        if not self.callback:
            return
        self.callback(index=index, *self.args, **self.kwargs)


class CallbackCheckBox(QtGui.QCheckBox):
    """
    Dynamic callback check box
    Will pass along the new value as a key-argument "value" to the callback function
    """
    def __init__(self, callback=None, checked=False, tool_tip=None, label=None, *args, **kwargs):
        QtGui.QCheckBox.__init__(self)
        self.callback = callback
        self.args = args
        self.kwargs = kwargs

        self.setChecked(checked)
        if tool_tip:
            self.setToolTip(tool_tip)

        if label:
            self.setText(label)
        else:
            self.setFixedWidth(15)

        self.connect(self, QtCore.SIGNAL("toggled(bool)"), self.toggled_event)

    def toggled_event(self, state):
        if not self.callback:
            return
        self.kwargs['value'] = state
        self.callback(*self.args, **self.kwargs)


class CallbackMenuAction(QtGui.QAction):
    """
    Dynamic QAction item for custom QMenu
    """
    def __init__(self, text, icon=None, callback=None, *args, **kwargs):
        if icon:
            QtGui.QAction.__init__(self, icon, text, None)
            self.setIconVisibleInMenu(True)
        else:
            QtGui.QAction.__init__(self, text, None)

        self.callback = callback
        self.args = args
        self.kwargs = kwargs

        self.triggered.connect(self.trigger_event)

    def trigger_event(self):
        if not self.callback:
            return
        self.callback(*self.args, **self.kwargs)


class SearchAndReplaceDialog(QtGui.QDialog):
    """
    Search and replace dialog window
    """
    __SEARCH_STR__ = ''
    __REPLACE_STR__ = ''

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.apply = False

        self.search_widget = None
        self.replace_widget = None

        self.setup()

    def setup(self):
        """
        Build/Setup the dialog window
        """
        self.setWindowTitle('Search And Replace')
        self.setFixedWidth(250)

        # Add layout
        main_layout = QtGui.QVBoxLayout(self)

        input_layout = QtGui.QHBoxLayout()
        main_layout.addLayout(input_layout)

        # Add labels
        labels_layout = QtGui.QVBoxLayout()

        search_label = QtGui.QLabel('Search')
        search_label.setFixedWidth(50)
        labels_layout.addWidget(search_label)

        replace_label = QtGui.QLabel('Replace')
        replace_label.setFixedWidth(50)
        labels_layout.addWidget(replace_label)

        input_layout.addLayout(labels_layout)

        # Add line edits
        text_layout = QtGui.QVBoxLayout()

        self.search_widget = QtGui.QLineEdit()
        self.search_widget.setText(self.__SEARCH_STR__)
        text_layout.addWidget(self.search_widget)

        self.replace_widget = QtGui.QLineEdit()
        self.replace_widget.setText(self.__REPLACE_STR__)
        text_layout.addWidget(self.replace_widget)

        input_layout.addLayout(text_layout)

        # Add buttons
        btn_layout = QtGui.QHBoxLayout()
        main_layout.addLayout(btn_layout)

        ok_btn = CallbackButton(callback=self.accept_event)
        ok_btn.setText('Ok')
        btn_layout.addWidget(ok_btn)

        cancel_btn = CallbackButton(callback=self.cancel_event)
        cancel_btn.setText('Cancel')
        btn_layout.addWidget(cancel_btn)

        ok_btn.setFocus()

    def accept_event(self):
        """
        Accept button event
        """
        self.apply = True

        self.accept()
        self.close()

    def cancel_event(self):
        """
        Cancel button event
        """
        self.apply = False
        self.close()

    def get_values(self):
        """
        Return field values and button choice

        :return type: list
        """
        search_str = unicode(self.search_widget.text())
        replace_str = unicode(self.replace_widget.text())
        if self.apply:
            SearchAndReplaceDialog.__SEARCH_STR__ = search_str
            SearchAndReplaceDialog.__REPLACE_STR__ = replace_str
        return search_str, replace_str, self.apply

    @classmethod
    def get(cls):
        """
        Default method used to run the dialog input window
        Will open the dialog window and return input texts.

        :return type: list
        """
        win = cls()
        win.exec_()
        win.raise_()
        return win.get_values()


class ProgressBarWindow(QtGui.QMainWindow):
    """
    Quick window for progress bar support
    """
    __DEFAULT_WIDTH__ = 200
    __DEFAULT_HEIGHT__ = 80

    def __init__(self,
                 label='Processing...',
                 size=1,
                 parent=None):
        QtGui.QMainWindow.__init__(self, parent)

        self._text = label

        # Main window setting
        self.setWindowTitle('Please Wait')
        self.resize(self.__DEFAULT_WIDTH__, self.__DEFAULT_HEIGHT__)

        # Add main widget and vertical layout
        main_widget = QtGui.QWidget(self)
        main_vertical_layout = QtGui.QVBoxLayout(main_widget)

        # Add processing label
        self.label = QtGui.QLabel(self)
        self.label.setAlignment(QtCore.Qt.AlignHCenter)
        main_vertical_layout.addWidget(self.label)

        # Create Process progress bar
        self.progress_bar = QtGui.QProgressBar()
        main_vertical_layout.addWidget(self.progress_bar)

        # Add bottom spacer
        spacer = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        main_vertical_layout.addItem(spacer)

        # # Add main widget to window
        self.setCentralWidget(main_widget)

        # Set Maximum size
        self.maximum = size or 1

    def show(self):
        """
        Overload default show definition to center on screen
        """
        self.center_on_screen()
        QtGui.QMainWindow.show(self)

    # # Doesn't seem to work while maya is in a loop ...
    # def keyPressEvent(self, event):
    #     if event.key() == QtCore.Qt.Key_Escape:
    #         self.close()

    def center_on_screen(self):
        """
        Position window on center screen
        """
        desktop_rect = QtGui.QDesktopWidget().screenGeometry(1)
        center_pt = desktop_rect.center()

        x_position = center_pt.x() - (.5 * self.width())
        y_position = center_pt.y() - (.5 * self.height())

        self.move(x_position, y_position)

    @property
    def maximum(self):
        return self.progress_bar.maximum()

    @maximum.setter
    def maximum(self, value):
        self.progress_bar.reset()
        self.progress_bar.setRange(1, value)

    @property
    def value(self):
        return self.progress_bar.value()

    @value.setter
    def value(self, value):
        # Update label text
        text = '{} {}/{}'.format(self._text, value, self.maximum)
        self.label.setText(text)

        # Update progress bar value
        self.progress_bar.setValue(value)

        # Close progress bar on end
        if value == self.maximum:
            self.close()
