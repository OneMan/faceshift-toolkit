# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

from maya import cmds
from maya import OpenMaya
from maya import OpenMayaAnim


class Node(object):
    """
    Small Maya Api node interface
    """

    def __init__(self, node_name):
        """
        :param node_name: name for the node
        """
        self._handle = OpenMaya.MObjectHandle()
        mboject = self.mobject_from_name(node_name)
        self.object = mboject

    def __eq__(self, other):
        """
        Sets and lists filtering support
        """
        if isinstance(other, self.__class__):
            return self.object == other.object
        elif isinstance(other, OpenMaya.MObject):
            return self.object == other
        elif hasattr('asMobject', other):
            return self.object == other.asMobject()
        else:
            return self.__hash__() == hash(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        if self.is_valid():
            return "%s.%s(u'%s')" % (self.__class__.__module__,
                                     self.__class__.__name__,
                                     self.name)
        else:
            return "%s.%s(None)" % (self.__class__.__module__,
                                    self.__class__.__name__)

    def __str__(self):
        return self.name

    def __add__(self, other):
        return '%s%s' % (self.name, unicode(other))

    def __radd__(self, other):
        return '%s%s' % (unicode(other), self.name)

    def __hash__(self):
        if self.is_valid():
            return hash(self.full_name())
        else:
            return hash(self.object)

    @staticmethod
    def mobject_from_name(node_name):
        """
        Return MObject for string name

        :param str node_name: name of the specified MObject
        :return type: OpenMaya.MObject
        """
        if isinstance(node_name, OpenMaya.MObject):
            return node_name
        elif hasattr(node_name, 'as_mobject'):
            return node_name.as_mobject()

        assert cmds.objExists(node_name), 'Node %s does not exist' % node_name

        sel_list = OpenMaya.MSelectionList()
        OpenMaya.MGlobal.getSelectionListByName(node_name, sel_list)
        mobject = OpenMaya.MObject()
        sel_list.getDependNode(0, mobject)

        return mobject

    @property
    def object(self):
        """
        Return the MObject associated with this instance

        :return type: OpenMaya.MObject
        """
        return self._handle.object()

    @object.setter
    def object(self, obj):
        """
        Set the MObject for thins Node instance
        """
        # Get MObject for input
        assert isinstance(obj, OpenMaya.MObject), 'input is not an MObject'

        # Update handle
        self._handle = OpenMaya.MObjectHandle(obj)

    def is_valid(self):
        """
        Check that associated MObject is still valid and alive

        :return type: bool
        """
        return self._handle.isValid() and self._handle.isAlive()

    def is_dag_node(self):
        """
        Return if this instance is a MDagNode or not

        :return type: bool
        """
        if not self.is_valid():
            return False
        if self.has_fn(OpenMaya.MFn.kDagNode):
            return True
        return False

    def _assert_dag_node(self):
        """
        Assert that this instance is a MDagNode
        """
        assert self.is_dag_node(), "Function only relevant to dagNodes!"

    def has_fn(self, fn):
        """
        Return the this instance is compatible with specified MFn type

        :param OpenMaya.MFn fn: function to test compatibility
        :return type: bool
        """
        return self.object.hasFn(fn)

    def mfn_dependency_node(self):
        """
        Return the corresponding MFnDependencyNode for this instance

        :return type: OpenMaya.MFnDependencyNode
        """
        dependency_node = OpenMaya.MFnDependencyNode()
        dependency_node.setObject(self.object)
        return dependency_node

    def m_dag_path(self):
        """
        Return the corresponding MDagPath for this instance

        :return type: OpenMaya.MDagPath
        """
        self._assert_dag_node()
        return OpenMaya.MDagPath.getAPathTo(self.object)

    def mfn_dag_node(self):
        """
        Return the corresponding MFnDagNode for this instance

        :return type: OpenMaya.MFnDagNode
        """
        return OpenMaya.MFnDagNode(self.m_dag_path())

    def mfn_transform(self):
        """
        Return the corresponding MFnTransform for this instance

        :return type: OpenMaya.MFnTransform
        """
        return OpenMaya.MFnTransform(self.m_dag_path())

    def mfn_mesh(self):
        """
        Return the corresponding MFnMesh for this instance

        :return type: OpenMaya.MFnMesh
        """
        assert self.has_fn(OpenMaya.MFn.kMesh), "Object has no functions 'kMesh'"
        mesh_function = OpenMaya.MFnMesh()
        mesh_function.setObject(self.m_dag_path())
        return mesh_function

    def mfn_blend_shape_deformer(self):
        """
        Return the corresponding MFnBlendShapeDeformer for this instance

        :return type: OpenMayaAnim.MFnBlendShapeDeformer
        """
        assert self.has_fn(OpenMaya.MFn.kBlendShape), "Object has no functions 'kBlendShape'"
        assert OpenMayaAnim.MFnBlendShapeDeformer().hasObj(self.object), 'Object is not a valid blend shape node'
        return OpenMayaAnim.MFnBlendShapeDeformer(self.object)

    def world_position(self):
        """
        Return the world position vector of this object

        :return type: OpenMaya.MVector
        """
        return self.mfn_transform().getTranslation(OpenMaya.MSpace.kWorld)

    def full_name(self):
        """
        Return the full name (with path) for this object

        :return type: str
        """
        if self.is_dag_node():
            return self.mfn_dag_node().fullPathName()
        return self.name

    def short_name(self):
        """
        Return the short name for this object

        :return type: str
        """
        if self.is_dag_node():
            return self.full_name().rsplit("|", 1)[-1]
        return self.name

    @property
    def name(self):
        """
        Return the short name of this object or the long name if it is not an unique name

        :return type: str
        """
        if not self.is_valid():
            return None
        if not self.mfn_dependency_node().hasUniqueName():
            return self.full_name()
        return self.mfn_dependency_node().name()

    @name.setter
    def name(self, new_name):
        """
        Rename the corresponding object

        :param str new_name: new name for the object
        :return: Return the resulting name of the object after rename
        :return type: str
        """
        cmds.rename(self.name, new_name)

    def attr_mplug(self, attribute):
        """
        Return the specified attribute MPlug object

        :return type: OpenMaya.MPlug
        """
        try:
            plug = self.mfn_dependency_node().findPlug(attribute)
            return plug

        except:
            try:
                sel_list = OpenMaya.MSelectionList()
                OpenMaya.MGlobal.getSelectionListByName((self.full_name() + "." + attribute), sel_list)
                if not sel_list.length():
                    return

                plug = OpenMaya.MPlug()
                sel_list.getPlug(0, plug)
                return plug
            except:
                return

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Vector and matrices methods   ---
    @property
    def matrix(self):
        """
        Return the local matrix of the object

        :return type: OpenMaya.MMatrix
        """
        return self.world_matrix() * self.parent_inverse_matrix()

    @matrix.setter
    def matrix(self, matrix):
        """
        Set the local matrix of the object

        :param OpenMaya.MMatrix matrix: matrix
        """
        self._assert_dag_node()
        trans_matrix = OpenMaya.MTransformationMatrix(matrix)
        trans = self.mfn_transform()
        trans.set(trans_matrix)

    def world_matrix(self):
        """
        Return the world MMatrix of the object

        :return type: OpenMaya.MMatrix
        """
        return self.m_dag_path().inclusiveMatrix()

    def world_inverse_matrix(self):
        """
        Return the world inverse MMatrix of the object

        :return type: OpenMaya.MMAtrix
        """
        return self.m_dag_path().inclusiveMatrixInverse()

    def parent_matrix(self):
        """
        Return the parent world MMatrix of the object

        :return type: OpenMaya.MMAtrix
        """
        return self.m_dag_path().exclusiveMatrix()

    def parent_inverse_matrix(self):
        """
        Return the parent world inverse MMAtrix of the object

        :return type: OpenMaya.MMAtrix
        """
        self._assert_dag_node()
        return self.m_dag_path().exclusiveMatrixInverse()

    def world_x_vector(self):
        """
        Return the world X MVector of the object

        :return type: OpenMaya.MVector
        """
        matrix = self.world_matrix()
        return OpenMaya.MVector(matrix(0, 0), matrix(0, 1), matrix(0, 2))

    def world_y_vector(self):
        """
        Return the world Y MVector of the object

        :return type: OpenMaya.MVector
        """
        matrix = self.world_matrix()
        return OpenMaya.MVector(matrix(1, 0), matrix(1, 1), matrix(1, 2))

    def world_z_vector(self):
        """
        Return the world Z MVector of the object

        :return type: OpenMaya.MVector
        """
        matrix = self.world_matrix()
        return OpenMaya.MVector(matrix(2, 0), matrix(2, 1), matrix(2, 2))

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Maya quickies   ---
    def shape(self, ni=True):
        """
        Return the shape of the object is any.

        :return type: Node
        """
        if self.has_fn(OpenMaya.MFn.kShape):
            return self

        shapes = cmds.listRelatives(self.name,
                                    ni=ni,
                                    children=True,
                                    fullPath=True,
                                    type='shape')
        if not shapes:
            return None
        return Node(shapes[0])

    @property
    def parent(self):
        """
        Return the parent of this object is any.

        :return type: Node
        """
        self._assert_dag_node()

        # Return None if world
        if self.m_dag_path().length() == 1:
            return None

        # Return parent
        return Node(self.mfn_dag_node().parent(0))

    @parent.setter
    def parent(self, other):
        """
        Parent the object to an other

        :param other: object to parent under
        """
        self._assert_dag_node()
        cmds.parent(self.name, other)


class BlendShapeNode(Node):
    """
    BlendShape Node api handler
    """
    def __init__(self, node_name):
        """
        :param node_name: node of the blend shape
        """
        Node.__init__(self, node_name)
        self.__mfn = None
        self._mfn = self.mfn_blend_shape_deformer()

        self.__target_item_mplug = dict()
        self.__input_geometry_mplug = dict()

    def __len__(self):
        """
        Return targets count number, do not use this to parse targets.
        Use the indexes property instead.

        :return type: int
        """
        return self._mfn.numWeights()

    @property
    def _mfn(self):
        assert self.is_valid(), 'blend shape does not seem to be valid any more'
        return self.__mfn
    
    @_mfn.setter
    def _mfn(self, mfn):
        self.__mfn = mfn

    @property
    def indexes(self):
        """
        Return the valid used indexes (deleted target index will mess up len() method)
        You should use this method to loop over targets.

        :return type: MIntArray
        """
        indexes = OpenMaya.MIntArray()
        self._mfn.weightIndexList(indexes)
        return indexes

    def next_index(self):
        """
        Return the next valid index (can be a deleted index)

        :return type: int
        """
        # Get used index list
        indexes = self.indexes
        if not indexes:
            return 0

        # Return len if last used index is len() -1
        if indexes[-1] == len(self) - 1:
            return len(self)

        # Parse index list to find first hole
        new_index = None
        i = 0
        while new_index is None:
            if not i in indexes:
                new_index = i
                break
            i += 1

        return new_index

    def bounding_box(self, geom_index):
        """
        Return the OpenMaya.MBoundingBox for the specified geometry index input mesh

        :param int geom_index: geometry index
        :return type: OpenMaya.MBoundingBox
        """
        node = Node(self.deformed_objects()[geom_index])
        dag_node = node.mfn_dag_node()
        return dag_node.boundingBox()

    def reset_all_targets(self):
        """
        Will reset blendShape target weights to 0
        """
        for i in self.indexes:
            self._mfn.setWeight(i, 0)

    def _get_weight_mplug(self, index):
        weight_attr = 'w[{}]'.format(index)
        return self.attr_mplug(weight_attr)

    def set_index_weight(self, index, weight=0.0):
        """
        Will set specified target index weight

        :param int index: target index
        :param float weight: new weight value
        """
        # Check that index is valid
        if index is None:
            return
        if not index in self.indexes:
            return

        # Set weight value
        self._mfn.setWeight(index, weight)

    def set_alias_weight(self, alias, weight=0.0):
        """
        Set the attribute weight for the specified target alias

        :param str alias: target alias
        :param float weight: weight
        """
        index = self.alias_index(alias)
        if index is None:
            return
        self.set_index_weight(index, weight)

    def index_weight(self, index):
        """
        Return the value of the weight index

        :param int index: target index
        :return type: float
        """
        return self._mfn.weight(index)

    def alias_weight(self, alias):
        """
        Return the attribute weight for the specified target alias

        :param str alias: target alias
        :return type: float
        """
        index = self.alias_index(alias)
        if index is None:
            return
        return self.index_weight(index)

    def index_alias(self, index):
        """
        Return target alias for specified index

        :param int index: target index
        :return type: unicode
        """
        return self._mfn.plugsAlias(self.attr_mplug('w[%d]' % index))

    def aliases(self):
        """
        Will return the alias name list for every target

        :return type: list
        """
        aliases = list()
        if not self._mfn.getAliasList(aliases):
            return list()

        filtered = [aliases[i] for i in range(0, len(aliases), 2)]
        filtered.sort()
        return filtered

    def alias_index(self, alias):
        """
        Return the index of the specified target alias

        :param str alias: alias name of the target shape
        :return type: int
        """
        plug = self.attr_mplug(alias)
        if not plug:
            return
        return plug.logicalIndex()

    def rename_target(self, alias, new_alias):
        """
        Rename a target alias

        :param str alias: name of the target to rename
        :param str new_alias: new name to use
        :return: alias
        """
        # Skip if alias is already used
        if new_alias in self.aliases():
            return alias

        # Get target plug
        plug = self.attr_mplug(alias)
        if not plug:
            return

        # Change alias
        cmds.aliasAttr(new_alias, plug.partialName(True))
        return new_alias

    def deformed_objects(self):
        """
        Return the deformed object list

        :return type: list
        """
        mobjects = OpenMaya.MObjectArray()
        self._mfn.getBaseObjects(mobjects)

        nodes = list()
        for i in range(mobjects.length()):
            nodes.append(Node(mobjects[i]))
        return nodes

    def shape_iterator(self, geom_index=0):
        """
        Return geom index input mesh geometry iterator

        :param int geom_index: deformed mesh geom index
        :return type: OpenMaya.MItGeometry
        """
        deformed_shapes = self.deformed_objects()
        if not deformed_shapes:
            return
        return OpenMaya.MItGeometry(deformed_shapes[geom_index].object)


    @staticmethod
    def _plug_mesh_data(mplug):
        """
        Get the MFnMeshData from a mesh MPlug

        :param OpenMaya.MPlug mplug: plug to read the mesh data from
        :return type: OpenMaya.MFnMeshData
        """
        assert isinstance(mplug, OpenMaya.MPlug), 'input is not a valid MPlug'
        return OpenMaya.MFnMeshData(mplug.asMObject())

    def _input_mesh_mplug(self, geom_index=0):
        """
        Return the MPlug object pointing to the input attribute

        :param int geom_index: geometry index
        :return type: OpenMaya.MPlug
        """
        attribute = 'input[{}].inputGeometry'.format(geom_index)
        return self.attr_mplug(attribute)

    def _input_geometry_target_mplug(self, target_index, geom_index=0):
        """
        Return the MPlug object pointing to the inputGeometry attribute

        :param int target_index: target index
        :param int geom_index: geometry index
        :return type: OpenMaya.MPlug
        """
        attribute = 'inputTarget[{}].inputTargetGroup[{}].inputTargetItem[6000].inputGeomTarget'.format(geom_index, target_index)
        return self.attr_mplug(attribute)

    def _input_target_offset_points_mplug(self, target_index, geom_index=0):
        """
        Return the MPlug object pointing to the inputPointsTarget MFnPointArrayData attribute

        :param int target_index: target index
        :param int geom_index: geometry index
        :return type: OpenMaya.MPlug
        """
        attribute = 'inputTarget[{}].inputTargetGroup[{}].inputTargetItem[6000].inputPointsTarget'.format(geom_index, target_index)
        return self.attr_mplug(attribute)

    def input_target_offset_points_values(self, target_index, geom_index=0):
        """
        Will return the stored MVectorArray of the offset values for the moved indexes of the target

        :param int target_index: target index
        :param int geom_index: geometry index
        :return type: OpenMaya.MVectorArray
        """
        mvectors = OpenMaya.MVectorArray()

        # Get attribute mplug
        plug = self._input_target_offset_points_mplug(target_index, geom_index)
        data = plug.asMDataHandle().data()
        if data.isNull():
            return mvectors

        # Get offset points array
        fn = OpenMaya.MFnPointArrayData(data)
        mpoints = OpenMaya.MPointArray()
        fn.copyTo(mpoints)

        # Convert to MVectorArray
        for i in range(mpoints.length()):
            mvectors.append(OpenMaya.MVector(mpoints[i]))

        return mvectors

    def _input_target_offset_components_mplug(self, target_index, geom_index=0):
        """
        Return the MPlug object pointing to the inputComponentsTarget MFnComponentListData attribute

        :param int target_index: target index
        :param int geom_index: geometry index
        :return type: OpenMaya.MPlug
        """
        attribute = 'inputTarget[{}].inputTargetGroup[{}].inputTargetItem[6000].inputComponentsTarget'.format(geom_index, target_index)
        return self.attr_mplug(attribute)

    def input_target_offset_components_indexes(self, target_index, geom_index=0):
        """
        Will return the stored offset components indexes of the target

        :param int target_index: target index
        :param int geom_index: geometry index
        :return type: list
        """
        indexes = list()

        # Get attribute mplug
        plug = self._input_target_offset_components_mplug(target_index, geom_index)
        data = plug.asMDataHandle().data()
        if data.isNull():
            return indexes

        # Get data handler
        components_data = OpenMaya.MFnComponentListData(data)

        # Extract indexes
        for i in range(components_data.length()):
            index_array = OpenMaya.MIntArray()
            OpenMaya.MFnSingleIndexedComponent(components_data[i]).getElements(index_array)
            [indexes.append(index) for index in index_array]

        return indexes

    def _mesh_offset_data(self, mesh, geom_index=0):
        """
        Will return the MFnComponentListData and MPointArray  resulting from the difference between
        the mesh and the neutral shape.

        :param mesh: input mesh to compare to the neutral
        :param int geom_index: geometry index of the neutral shape
        :return type: MFnComponentListData, MPointArray
        """
        # Get mesh iterator
        mesh = Node(mesh).shape()
        mesh_iterator = OpenMaya.MItMeshVertex(mesh.object)

        # Get neutral iterator
        neutral_data = self.input_geometry_data(geom_index)
        neutral_iterator = OpenMaya.MItMeshVertex(neutral_data.object())

        # Quick check
        assert mesh_iterator.count() == neutral_iterator.count(), 'The mesh does not seem to match the neutral shape'

        # Compare components
        components_data = OpenMaya.MFnComponentListData()
        components_data.create()

        offset_values = OpenMaya.MPointArray()

        while not mesh_iterator.isDone():
            # Skip unchanged component
            if mesh_iterator.position() == neutral_iterator.position():
                mesh_iterator.next()
                neutral_iterator.next()
                continue

            # Add component to component list
            component = mesh_iterator.currentItem()
            components_data.add(component)

            # Get offset value
            offset_vector = mesh_iterator.position() - neutral_iterator.position()
            offset_values.append(OpenMaya.MPoint(offset_vector))

            # Increment iterator
            mesh_iterator.next()
            neutral_iterator.next()

        # Create Point array data
        offset_data = OpenMaya.MFnPointArrayData()
        offset_data.create(offset_values)

        return components_data, offset_data

    def add_target(self, mesh, target_alias=None, weight=1.0, geom_index=0):
        """
        Add a target or inbetween to the blend shape node

        :param str mesh: name of the target mesh
        :param str target_alias: target weight alias desired name
        :param float weight: weight of the target (for inbetweens)
        :param int geom_index: geometry index
        """
        # Sanity check
        assert cmds.objExists(mesh), 'You need to specify a valid mesh when adding a new target'

        # Convert target mesh
        mesh = Node(mesh)

        # Check Target alias
        if not target_alias:
            target_alias = mesh.short_name()
        assert not target_alias in self.aliases(), 'Target alias "{}" is already in use'.format(target_alias)

        # Get base object
        bases_objects = OpenMaya.MObjectArray()
        self._mfn.getBaseObjects(bases_objects)

        # Add the target
        target_index = self.next_index()
        self._mfn.addTarget(bases_objects[geom_index],
                             target_index,
                             mesh.shape().object,
                             weight)

        # Get new target plug
        target_mplug = self._get_weight_mplug(target_index)

        # Update the weight alias
        self._mfn.setAlias(target_alias,
                            'weight[{}]'.format(target_index),
                            target_mplug,
                            True)

    def remove_target(self, target_index, weight=1.0, geom_index=0):
        """
        Remove the specified target index from the blend shape node

        :param int target_index: target index
        :param float weight: target weight
        :param int geom_index: geometry index
        """
        # Sanity check
        assert target_index in self.indexes, 'Target index {} not found'.format(target_index)

        # Get base object
        bases_objects = OpenMaya.MObjectArray()
        self._mfn.getBaseObjects(bases_objects)

        # Get target mobject from inputs
        target_objects = OpenMaya.MObjectArray()
        self._mfn.getTargets(bases_objects[geom_index],
                              target_index,
                              target_objects)

        # Input is valid
        target_mesh = None
        if target_objects[0]:
            target_mesh = Node(target_objects[0])

        # No valid target input connection
        # (Do not ask why, a target needs to be connected to be removed ...)
        else:
            # Get the target name
            target_name = self.index_alias(target_index)

            # Get the target mesh already exists
            if cmds.objExists(target_name):
                target_mesh = Node(target_name).shape()

            # Generate the target shape
            else :
                target_mesh = self.generate_target_shape_by_index(target_index, geom_index=geom_index).shape()

            # Connect new mesh to blendShape
            target_mplug = target_mesh.attr_mplug('worldMesh[0]')
            node_mplug = self._input_geometry_target_mplug(target_index, geom_index=geom_index)

            dg_modifier = OpenMaya.MDGModifier()
            dg_modifier.connect(target_mplug, node_mplug)
            dg_modifier.doIt()

        # Remove target from blend shape node
        self._mfn.removeTarget(bases_objects[geom_index],
                                target_index,
                                target_mesh.object,
                                weight)

        # Delete target mesh
        if not target_objects[0]:
            cmds.delete(target_mesh.parent)

    def update_target_offset_data(self, target_index, mesh=None, geom_index=0):
        """
        Will Update the blend shape node with the new target offset data

        :param int target_index: index of the target to update
        :param str mesh: mesh to use for the update (if none will look for one with the target_name)
        :param int geom_index: geometry index to edit
        """
        # Creating a new shape
        if not target_index in self.indexes:
            return self.add_target(mesh)

        # Get target mesh
        target_alias = self.index_alias(target_index)
        if not target_alias:
            return
        if not mesh:
            mesh = target_alias

        # Check that target mesh exists
        assert cmds.objExists(mesh), 'Mesh "{}" does not exists'.format(mesh)

        # Get target input geometry mplug
        target_mplug = self._input_geometry_target_mplug(target_index, geom_index)

        # Break any input connection
        if target_mplug.isConnected():
            # Get connections
            connected_mplugs = OpenMaya.MPlugArray()
            target_mplug.connectedTo(connected_mplugs, True, False)

            # Break connections
            if connected_mplugs:
                # Live target case
                node = Node(connected_mplugs[0].node())
                if node.name == target_alias:
                    return

                # Disconnect
                OpenMaya.MDGModifier().disconnect(connected_mplugs[0], target_mplug)

        # Get offset data
        components_data, offset_data = self._mesh_offset_data(mesh, geom_index)

        # # Update component attribute
        components_mplug = self._input_target_offset_components_mplug(target_index, geom_index)
        components_mplug.setMObject(components_data.object())

        # Update MPoints data
        offset_mplug = self._input_target_offset_points_mplug(target_index, geom_index)
        offset_mplug.setMObject(offset_data.object())

    def _output_mesh_mplug(self, geom_index=0):
        """
        Get the output geometry attribute MPlug for the specified geometry index.

        :param int geom_index: geometry index
        :return type: OpenMaya.MPlug
        """
        # Get the input MPlugArray
        output_plug = self.attr_mplug('outputGeometry')
        output_plug = output_plug.elementByLogicalIndex(geom_index)
        if not output_plug:
            return

        return output_plug

    def _target_mesh_data(self, target_index, geom_index=0):
        """
        Will read the offset data from the target input and apply it to the neutral shape data and return it

        :param int target_index: index of the target to process
        :param int geom_index: geometry index
        :return type: OpenMaya.MFnMeshData
        """
        # Get input shape data
        input_data = self.input_geometry_data(geom_index)

        # Duplicate input geometry data
        data = OpenMaya.MFnMeshData()
        data_obj = data.create()
        OpenMaya.MFnMesh().copy(input_data.object(), data_obj)

        # Get target offset information
        offset_indexes = self.input_target_offset_components_indexes(target_index, geom_index)
        offset_values = self.input_target_offset_points_values(target_index, geom_index)

        # Initialize iterator
        mesh_iterator = OpenMaya.MItMeshVertex(data.object())

        # Init previous index pointer
        util = OpenMaya.MScriptUtil()
        util.createFromInt(0)
        prev_index = util.asIntPtr()

        # Apply offsets
        for i in range(len(offset_indexes)):
            # Set iterator index
            mesh_iterator.setIndex(offset_indexes[i], prev_index)

            # Add offset to position
            mesh_iterator.setPosition(mesh_iterator.position() + offset_values[i])

        return data

    @staticmethod
    def _create_mesh_from_data(data, name=None):
        """
        Will create a new named mesh from MPlug data

        :param OpenMaya.MFnMeshData data: mesh data
        :return type: Node
        """
        assert isinstance(data, (OpenMaya.MFnMeshData, OpenMaya.MFnMesh)),\
            'data is not of type OpenMaya.MFnMeshData|OpenMaya.MFnMesh'

        # Create new Mesh from data
        new_mesh = Node(OpenMaya.MFnMesh().copy(data.object()))

        # Rename new mesh
        if name:
            new_mesh.name = name

        # Assign initial shading group
        cmds.sets(new_mesh, e=True, fe='initialShadingGroup')

        return new_mesh

    def input_geometry_data(self, geom_index=0):
        """
        Will parse the dependency graph for input connected shape,
        avoiding any tweak error deformation on the neutral shape

        :param int geom_index: geometry index
        :return type: OpenMaya.MFnMeshData
        """
        # Get input mesh mplug
        mplug = self._input_mesh_mplug(geom_index)
        if not mplug.isConnected():
            return

        # Define iterator settings
        direction = OpenMaya.MItDependencyGraph.kUpstream
        traversal = OpenMaya.MItDependencyGraph.kBreadthFirst
        filter_type = OpenMaya.MFn.kMesh
        level = OpenMaya.MItDependencyGraph.kPlugLevel

        # Initialize dependency graph iterator
        iterator = OpenMaya.MItDependencyGraph(mplug,
                                               filter_type,
                                               direction,
                                               traversal,
                                               level)

        # No shape found
        if iterator.isDone():
            return

        # Get first shape
        input_mesh = Node(iterator.currentItem())

        # Get mesh data
        return input_mesh.mfn_mesh()

    def generate_neutral_shape(self, new_name='neutral', geom_index=0):
        """
        Will generate a duplicate of the neutral shape (base on input geom_index mesh data before deformation)

        :param str new_name: name for the new mesh
        :param int geom_index: geometry index of the input shape
        :return type: Node
        """
        # Get input shape data
        data = self.input_geometry_data(geom_index)
        if not data:
            return

        # Create new mesh from data
        mesh = self._create_mesh_from_data(data, new_name)

        return mesh

    def generate_target_shape_by_index(self, target_index, new_name=None, geom_index=0, connect=True):
        """
        Will generate a duplicate of target shape based on its index and stored deltas

        :param int target_index: target index
        :param str new_name: new name for the target (optional)
        :param int geom_index: geometry index
        :param bool connect: connect or not the shape to the blend shape target input
        :return type: Node
        """
        # Define mesh name
        if not new_name:
            new_name = self.index_alias(target_index)

        # Delete existing mesh
        if cmds.objExists(new_name):
            cmds.delete(new_name)

        # Get input shape data
        data = self._target_mesh_data(target_index, geom_index)

        # Create new mesh from data
        mesh = self._create_mesh_from_data(data, new_name)

        # Connect blend shape target input
        if connect:
            mesh_mplug = mesh.attr_mplug('worldMesh[0]')
            target_mplug = self._input_geometry_target_mplug(target_index, geom_index=geom_index)

            dg_modifier = OpenMaya.MDGModifier()
            dg_modifier.connect(mesh_mplug, target_mplug)
            dg_modifier.doIt()

        return mesh
