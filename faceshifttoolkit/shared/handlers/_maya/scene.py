# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import re

from maya import cmds
from maya import OpenMaya
from faceshifttoolkit.shared.handlers._maya.api import Node


class Grid(object):
    """
    Class handling maya bounding boxes and node positioning
    """
    def __init__(self, base_nodes, column_count=10, factor=1.5):
        """
        :param list base_nodes: list of the reference boundingbox nodes or a single node
        :param int column_count: number of columns to use
        :param float factor: spacing factor to use
        """
        self.base_nodes = base_nodes
        self.column_count = column_count
        self.factor = factor

        self._skip_list = None

    @staticmethod
    def filter_node(node):
        """
        Will return a transform for the specified node (the parent if a shape is specified)

        :param str node: node name
        :return: Node
        """
        node = Node(node)
        if node.has_fn(OpenMaya.MFn.kShape):
            return node.parent
        return node

    def base_bounding_box(self):
        """
        Return the current bounding box for the base node

        :return: OpenMaya.MBoundingBox
        """
        # Bounding box case
        if isinstance(self.base_nodes, OpenMaya.MBoundingBox):
            return self.base_nodes

        # List of nodes case
        if isinstance(self.base_nodes, list):
            bounding_box = OpenMaya.MBoundingBox()
            for node in self.base_nodes:
                bounding_box.expand(Node(node).mfn_dag_node().boundingBox())
            return bounding_box

        # Single node case
        return Node(self.base_nodes).mfn_dag_node().boundingBox()

    def _meshes_bounding_boxes(self):
        """
        Return a generator to iterate over every shape bounding box in the scene

        :return: generator
        """
        for mesh in cmds.ls(type='mesh', r=True):
            if mesh in self._skip_list:
                continue
            mesh = Node(mesh)
            bounding_box = mesh.mfn_dag_node().boundingBox()
            bounding_box.transformUsing(mesh.parent_matrix())
            yield bounding_box

    def _grid_bounding_boxes(self):
        """
        Return a generator for the grid bounding boxes

        :return: generator
        """
        # Get base bounding box information
        base_bounding_box = self.base_bounding_box()
        width = base_bounding_box.width()
        height = base_bounding_box.height()

        # Process
        row = 0
        while True:
            # Parse columns
            for column in range(self.column_count):
                # Get offset vector
                offset_vector = OpenMaya.MVector(width * (column +1) * self.factor,
                                                 -1 * height * row * self.factor,
                                                 0.0)

                # Build bounding box
                bounding_box = OpenMaya.MBoundingBox(base_bounding_box.min() + offset_vector,
                                                     base_bounding_box.max() + offset_vector)

                yield bounding_box

            # Increment row
            row += 1

    def next(self):
        """
        Return next unoccupied grid bounding box space

        :return: OpenMaya.MBoundingBox
        """
        # Initialize mesh bounding box list
        meshes_boxes = list(self._meshes_bounding_boxes())

        # Parse valid defined grid bounding boxes to find an empty slot
        for grid_box in self._grid_bounding_boxes():
            intersects = False

            # Compare bounding box with each mesh bounding box
            for mesh_box in meshes_boxes:
                if not grid_box.intersects(mesh_box):
                    continue
                intersects = True
                break

            # Skip if current bounding box intersected with an existing mesh
            if intersects:
                continue

            # Return free space bounding box
            return grid_box

    def _add_to_skip_list(self, node):
        self._skip_list = list()
        if node.has_fn(OpenMaya.MFn.kMesh):
            self._skip_list.append(node)
            return

        self._skip_list = cmds.listRelatives(node, ad=True, type='mesh')

    def set_position(self, node):
        """
        will set the position of the node to the next free grid space.
        """
        node = self.filter_node(node)

        # Update skip list
        self._add_to_skip_list(node)

        # Get node bounding box
        node_bounding_box = node.mfn_dag_node().boundingBox()
        node_bounding_box.transformUsing(node.parent_matrix())

        # Get next free space
        next_box = self.next()

        # Get pivot offset from bounding box center
        pivot_position = OpenMaya.MPoint(*cmds.xform(node, q=True, ws=True, rp=True))
        pivot_offset = pivot_position - node_bounding_box.center()

        # Get node freeze offset (frozen with transforms ...)
        node_translate = OpenMaya.MPoint(*cmds.xform(node, q=True, t=True, ws=True))
        freeze_offset = pivot_position - node_translate

        # Get offset vector
        offset  = next_box.center() + pivot_offset - freeze_offset

        # Apply offset to node
        cmds.xform(node, ws=True, t=[offset.x, offset.y, offset.z])

        # Reset skip list
        self._skip_list = None

    def set_scale(self, node):
        """
        Will scale node hypothetically to match reference node based on bounding box average
        """
        node = self.filter_node(node)

        # Update skip list
        self._add_to_skip_list(node)

        # Get base bounding box scale vector
        base_bounding_box = self.base_bounding_box()
        base_scale_vector = OpenMaya.MVector(base_bounding_box.max().x - base_bounding_box.min().x,
                                             base_bounding_box.max().y - base_bounding_box.min().y,
                                             base_bounding_box.max().z - base_bounding_box.min().z)

        # Get node bounding box scale vector
        node_bounding_box = node.mfn_dag_node().boundingBox()
        node_bounding_box.transformUsing(node.parent_matrix())

        node_scale_vector = OpenMaya.MVector(node_bounding_box.max().x - node_bounding_box.min().x,
                                             node_bounding_box.max().y - node_bounding_box.min().y,
                                             node_bounding_box.max().z - node_bounding_box.min().z)

        # Define ratio scale vector
        ratio = base_scale_vector.length() / node_scale_vector.length()

        # Apply scale
        cmds.xform(node, s=[ratio,ratio, ratio])

        # Reset skip list
        self._skip_list = None


class Reference(object):
    """
    Class handling maya reference files
    """
    __NAMESPACE__ = 'template'
    __GROUP__ = 'TEMPLATE_GRP'

    def __init__(self, file_path):
        self._node = None

        self.__file_path = None
        self.file = file_path

        self.__commands = None
        self.commands = None

    def __eq__(self, other):
        return self.file == other or self.os_file_path == other

    def __repr__(self):
        return "%s.%s(u'%s')" % (self.__class__.__module__,
                                 self.__class__.__name__,
                                 self.file)

    def __hash__(self):
        return hash(self.os_file_path)

    @property
    def name(self):
        if not self.is_referenced():
            return self.file
        return self.node.name

    @property
    def group(self):
        if not cmds.objExists(self.__GROUP__):
            return None
        return Node(self.__GROUP__)

    @property
    def file(self):
        """
        The file path property

        :return type: str
        """
        return self.__file_path

    @file.setter
    def file(self, path):
        """
        The file path property setter

        :param str path: the file path
        """
        if not path:
            self.remove()
            self.__file_path = path
            return

        # Strip extra from file name
        os_path = re.sub(r'{.*}$', '', path)

        # Check path validity
        if not os.path.exists(os_path):
            sys.stderr.write('file "{}" not found\n'.format(os_path))
        path = path.replace(os.path.sep, '/')

        if self.file == path:
            if not self.is_referenced():
                self.load()
            if not self.is_enabled():
                self.enable()
            return

        self.__file_path = path

        # Update existing node
        if self._node:
            if not cmds.objExists(self._node):
                self._node = None
            else:
                self.load()
                return

        # Get existing matching reference node for new instance
        try:
            node = cmds.file(path, q=True, referenceNode=True)
        except:
            node = None

        # Update instance with existing reference node
        if node:
            self.node = node
            return

        # Create new reference
        self._new_reference()

    @property
    def commands(self):
        return self.__commands

    @commands.setter
    def commands(self, value):
        self.__commands = value

    @property
    def os_file_path(self):
        """
        Return a valid os file path, stripping any {*} from the end path that maya uses

        :return type: str
        """
        return re.sub(r'{.*}$', '', self.file)

    def _new_reference(self):
        """
        Load file as a new reference
        """
        # Get existing reference nodes
        references = cmds.ls(type='reference')

        # Create new reference
        cmds.file(self.os_file_path,
                  reference=True,
                  groupReference=True,
                  groupName=self.__GROUP__,
                  namespace=self.__NAMESPACE__)

        # Get new reference node
        for node in cmds.ls(type='reference'):
            if node in references:
                continue
            self.node = node
            break

        # Disable double transforms
        self.disable_double_transform()

        # Move out of bounding box
        self.move_from_bounding_box()

    def _update_reference(self):
        """
        Reload the reference, will update to new file if file path has changed.
        """
        if not self.node:
            return

        # Get current reference file
        current_file = cmds.referenceQuery(self.node, filename=True)
        if self.os_file_path == re.sub(r'{.*}$', '', current_file):
            return

        # Remove current reference
        cmds.file(self.os_file_path, removeReference=True, force=True)

        # Delete reference group
        if cmds.objExists(self.__GROUP__):
            cmds.delete(self.__GROUP__)

        # Create new reference
        self._new_reference()

    def load(self):
        """
        Reference this file in the scene
        """
        # Return if already loaded
        if self.is_referenced():
            return

        #First load case, create the reference node
        if not self.node:
            return self._new_reference()
        if not cmds.objExists(self.node):
            return self._new_reference()

        # Change existing reference path
        return self._update_reference()

    def refresh(self):
        pass

    def is_referenced(self):
        """
        Return if this file is referenced on not in the scene
        """
        if not self.node:
            return False
        return self.file and self in self.scene_references()

    def _assert_referenced_status(self):
        assert self.is_referenced(), 'the file "{}" is not referenced'.format(self.file)

    @property
    def node(self):
        """
        Return the reference node

        :return: Node
        """
        return self._node

    @node.setter
    def node(self, node):
        if not node:
            return
        if cmds.objExists(node) and cmds.nodeType(node) == 'reference':
            self._node = Node(node)

    @property
    def namespace(self):
        """
        Return the namespace used by the reference

        :return type: str
        """
        self._assert_referenced_status()
        return cmds.referenceQuery(self.node, namespace=True)

    @namespace.setter
    def namespace(self, name):
        self._assert_referenced_status()
        assert not cmds.namespace(ex=name), 'namespace "{}" already exists'.format(name)
        cmds.namespace(rename=[self.namespace, name])

    def is_enabled(self):
        """
        Return is the reference file is loaded

        :return type: bool
        """
        if not self.is_referenced():
            return False
        if not cmds.objExists(self.node):
            return False
        return cmds.referenceQuery(self.node, isLoaded=True)

    def enable(self):
        """
        Load the reference file
        """
        # Reference file if not referenced
        if not self.is_referenced():
            self.load()

        # Stop here if already loaded/visible
        if self.is_enabled():
            return

        # Load reference
        cmds.file(self.file, loadReference=self.node)

    def disable(self):
        """
        Unload the reference file
        """
        if not self.is_enabled():
            return
        cmds.file(self.file, unloadReference=self.node)

    def remove(self):
        """
        Remove the reference file
        """
        if not self.is_referenced():
            return
        cmds.file(self.file, removeReference=True, f=True)
        self._node = None

    def ls(self, *args, **kwargs):
        """
        List nodes in this namespace (see maya ls command for flags)

        :return type: list
        """
        if not self.is_enabled():
            return list()

        # Filter arg name
        name = '{}:'.format(self.namespace)
        if args:
            name += args[0]
        else:
            name += '*'

        return cmds.ls(name, **kwargs)

    def disable_double_transform(self):
        """
        Will disable the inherit transform flag on skinCluster deformed geometries
        """
        # Parse skin clusters
        for skin_cluster in self.ls(type='skinCluster'):
            # Parse deformed shapes
            for shape in cmds.skinCluster(skin_cluster, q=True, g=True):
                # Get transform
                shape = Node(shape)
                cmds.setAttr('{}.it'.format(shape.parent), False)

    def move_from_bounding_box(self, factor=-1.5):
        """
        Will offset the reference base group by a factor of the bounding box

        :param float factor: multiply factor
        """
        # Get base group
        group = self.group
        if not group:
            return

        # Init the grid instance
        if not self.commands:
            grid = Grid(group, factor=factor)

            # Move reference
            grid.set_position(group)
        else:
            self.commands.move_from_bounding_box(group, scale=True, factor=factor)

    @staticmethod
    def scene_references():
        """
        Return a list of Reference instance for each referenced file in the current scene

        :return type: list
        """
        # Get references nodes list
        ref_nodes = cmds.ls(rf=True)

        # Get Reference instances
        references = list()
        for node in ref_nodes:
            ref_file = cmds.referenceQuery(node, filename=True)
            references.append(Reference(ref_file))

        return references


# Init single reference handler
TEMPLATE_REFERENCE = Reference(None)
