# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

# Import application dependencies
from faceshifttoolkit.shared.handlers._maya.qt import *


# Main Qt support
try:
    from PySide import QtCore, QtGui, QtOpenGL
except:
    try:
        from PyQt4 import QtCore, QtGui, QtOpenGL
    except:
        raise Exception('Failed to import PyQt4 or Pyside')

try:
    import shiboken
except:
    try:
        from PySide import shiboken
    except:
        try:
            import sip
        except:
            raise Exception('Failed to import sip or shiboken')


# Instance handling
def wrap_instance(ptr, base=None):
    """
    Return QtGui object instance based on pointer address
    """
    if ptr is None:
        return None
    ptr = long(ptr)
    if 'shiboken' in globals():
        if base is None:
            q_obj = shiboken.wrapInstance(long(ptr), QtCore.QObject)
            meta_obj = q_obj.metaObject()
            cls = meta_obj.className()
            super_cls = meta_obj.superClass().className()
            if hasattr(QtGui, cls):
                base = getattr(QtGui, cls)
            elif hasattr(QtGui, super_cls):
                base = getattr(QtGui, super_cls)
            else:
                base = QtGui.QWidget
        return shiboken.wrapInstance(long(ptr), base)
    elif 'sip' in globals():
        base = QtCore.QObject
        return sip.wrapinstance(long(ptr), base)
    else:
        return None


def unwrap_instance(qt_object):
    """
    Return pointer address for qt class instance
    """
    if 'shiboken' in globals():
        return long(shiboken.getCppPointer(qt_object)[0])
    elif 'sip' in globals():
        return long(sip.unwrapinstance(qt_object))


def get_maya_window():
    """
    Get the program main window as a QMainWindow instance
    """
    try:
        ptr = get_main_window_ptr()
        return wrap_instance(long(ptr), QtGui.QMainWindow)
    except:
        # Fails at import on maya launch since ui isn't up yet
        return None


def ui_loader(window, new=False):
    """
    Load ui window
    """
    # Skip in batch mode
    if batch_mode():
        return

    # Sanity check
    assert hasattr(window, '__OBJ_NAME__'), 'window does not seem to be a valid object'

    # Get existing window
    dock_widget = None
    if not new:
        dock_pt = find_dock_window(window.__OBJ_NAME__)
        if dock_pt:
            # Get dock qt instance
            dock_widget = wrap_instance(long(dock_pt), QtGui.QDockWidget)

    # Create new window
    if not dock_widget:
        dock_widget = window(parent=get_maya_window())
        dock_widget.setWindowFlags(QtCore.Qt.Window)

    # Show ui
    dock_widget.show()
    dock_widget.raise_()

    return dock_widget
