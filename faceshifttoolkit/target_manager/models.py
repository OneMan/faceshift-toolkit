# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.


from faceshifttoolkit.shared.handlers.qt import QtCore
from faceshifttoolkit.shared.handlers.qt import QtGui


class TargetsTableModel(QtCore.QAbstractItemModel):
    """
    Targets QTableView template model
    """
    __SORT_ROLE__ = QtCore.Qt.UserRole
    __FILTER_ROLE__ = QtCore.Qt.UserRole + 1
    __DATA_ROLE__ = QtCore.Qt.UserRole + 2

    def __init__(self, root, view_widget=None, parent=None):
        """
        :param BlendShapeData root: the root data node
        :param QtCore.QtObject parent: parent object
        """
        QtCore.QAbstractItemModel.__init__(self, parent)
        self._data_root = root
        self._view_widget = view_widget

    def flags(self, index):
        """
        Return the configuration flags for the cell
        """
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def columnCount(self, parent):
        """
        Return the number of column to to display

        :return type: int
        """
        return 2

    def rowCount(self, parent):
        """
        Return the number of rows to display

        :return type: int
        """
        if not parent.isValid():
            if self._data_root:
                parent_node = self._data_root
            else:
                return 0
        else:
            parent_node = parent.internalPointer()

        return parent_node.count()

    def _generate_background(self, width, value):
        """
        Will generate the cell background representing the value of the target

        :param int width: width of the cell (image size)
        :param double value: Value of the blendShape (from 0 to 1)
        :return: QtGui.QImage
        """
        # Define colors
        color = QtGui.QColor(175, 255, 255, 35)
        transparent = QtGui.QColor(color.red(), color.green(), color.blue(), 0)
        scale_color = QtGui.QColor(255, 255, 255, 30)

        # Init image
        image = QtGui.QImage(width, 1, QtGui.QImage.Format_ARGB32)
        image.fill(transparent.rgba())

        if value:
            # Fill background color
            painter = QtGui.QPainter(image)
            painter.setPen(color)
            painter.drawLine(QtCore.QPoint(0, 0), QtCore.QPoint(int(value * width), 0))

            # Add value scale lines
            painter.setPen(scale_color)
            for pt in [0.25, 0.5, 0.75]:
                pt = QtCore.QPoint(int(pt * width), 0)
                painter.drawPoint(pt)

            painter.end()

        return image

    def data(self, index, role):
        node = index.internalPointer()
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return node.index
            elif column == 1:
                return node.name

        elif role == QtCore.Qt.BackgroundRole and column == 1:
            if not self._view_widget:
                return

            # Get cell size information
            width = self._view_widget.columnWidth(1)
            value = 0.0
            if node.is_valid():
                value = node.weight or 0.0

            # Get or generate cache pixmap
            pixmap = QtGui.QPixmap()
            cache_key = 'fstk_tm_{}_{}'.format(width, int(value * width))
            if not QtGui.QPixmapCache.find(cache_key, pixmap):
                pixmap.convertFromImage(self._generate_background(width, value))
                QtGui.QPixmapCache.insert(cache_key, pixmap)

            return QtGui.QBrush(pixmap)

        elif role == QtCore.Qt.ToolTipRole:
            return node.status_tooltip()

        elif role == QtCore.Qt.TextAlignmentRole and column == 0:
            return QtCore.Qt.AlignCenter

        elif role == QtCore.Qt.DecorationRole and column == 1:
            return QtGui.QIcon(node.status_icon())

        elif role == self.__SORT_ROLE__:
            if column == 0:
                return node.index
            elif column == 1:
                return node.name

        elif role == self.__FILTER_ROLE__:
            return node.name

        elif role == self.__DATA_ROLE__:
            return node

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if section == 1:
                return 'Target'
            else:
                return '#'

    def get_node(self, index):
        """
        Return the data node for the specified index

        :param QtCore.QModelIndex index: index
        :return: the data node
        """
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._data_root

    def index(self, row, column, parent):
        parent_node = self.get_node(parent)
        if not parent_node:
            return QtCore.QModelIndex()
        child = parent_node.child(row)

        if child:
            return self.createIndex(row, column, child)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        node = index.internalPointer()
        parent = node.parent()
        if parent is None:
            return QtCore.QModelIndex()
        else:
            return self.createIndex(parent.get_index(), 0, parent)
