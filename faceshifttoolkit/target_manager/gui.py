# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.


import os
import re
import time

from maya import cmds
from maya import OpenMaya

import faceshifttoolkit
from faceshifttoolkit.shared import ui
from faceshifttoolkit.shared.handlers import api
from faceshifttoolkit.shared.handlers import scene
from faceshifttoolkit.shared.handlers.qt import QtCore
from faceshifttoolkit.shared.handlers.qt import QtGui
from faceshifttoolkit import target_manager
from faceshifttoolkit.target_manager.commands import Commands
from faceshifttoolkit.target_manager.resources.icons import Icon
from faceshifttoolkit.target_manager import data
from faceshifttoolkit.target_manager import models


class CustomComboBox(ui.CallbackComboBox):
    """
    Callback combo bow with shared confirm input window method
    """
    def _confirm_input_window(self, title, message):
        """
        Opens a user input confirmation window, and return the user confirmation

        :return type: bool
        """
        # Open confirmation
        reply = QtGui.QMessageBox.question(self,
                                           title,
                                           message,
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return False

        return True


class BlendShapeComboBox(CustomComboBox):
    """
    Blend shape node combo box selector.
    """
    def contextMenuEvent(self, *args, **kwargs):
        """
        Context menu event with specific blend shape options
        """
        # Init context menu
        menu = QtGui.QMenu()

        add_separator = False
        if self.currentIndex() > -1:
            if cmds.objExists(self.currentText()):
                # Select blend shape node
                select_action = QtGui.QAction(QtGui.QIcon(Icon.SELECT), "Select", None)
                select_action.setIconVisibleInMenu(True)
                select_action.triggered.connect(self.select_node_event)
                menu.addAction(select_action)

                # Rename node
                rename_action = QtGui.QAction(QtGui.QIcon(Icon.EDIT), "Rename...", None)
                rename_action.setIconVisibleInMenu(True)
                rename_action.triggered.connect(self.rename_node_event)
                menu.addAction(rename_action)

                # Delete node
                delete_action = QtGui.QAction(QtGui.QIcon(Icon.CANCEL), "Delete", None)
                delete_action.setIconVisibleInMenu(True)
                delete_action.triggered.connect(self.delete_node_event)
                menu.addAction(delete_action)

                add_separator = True

        # Common
        if cmds.ls(sl=True):
            # Separator
            if add_separator:
                menu.addSeparator()

            # Create new node
            create_action = QtGui.QAction(QtGui.QIcon(Icon.ADD), "Create...", None)
            create_action.setIconVisibleInMenu(True)
            create_action.triggered.connect(self.create_new_event)
            menu.addAction(create_action)

        # exec the menu and map to position
        menu.exec_(QtGui.QCursor.pos())

    def populate(self, active_name=None):
        """
        Will populate the combo box with existing blendShapes nodes from scene.
        """
        new_index = None
        if not active_name:
            active_name = self.currentText()

        # Reset
        self.clear()

        # Get blend shape list
        blend_shapes = cmds.ls(type='blendShape')

        # Get blend shapes nodes list
        for i in range(len(blend_shapes)):
            # Skip referenced blend node
            if cmds.referenceQuery(blend_shapes[i], isNodeReferenced=True):
                continue

            # Add item to combo box
            self.addItem(blend_shapes[i])

            # Check for default active shape
            if blend_shapes[i] == active_name:
                new_index = i

        # Set active index
        if new_index:
            self.setCurrentIndex(new_index)

    def select_node_event(self):
        """
        Will select the corresponding blend shape node
        """
        cmds.select(self.currentText())

    def delete_node_event(self):
        """
        Will delete the corresponding blend shape node
        """
        if not self.count():
            return

        # Open confirmation window
        title = 'Delete BlendShape'
        message = 'Are you sure you want to delete\n"{}" ?'.format(self.currentText())
        if not self._confirm_input_window(title, message):
            return

        # Delete node
        cmds.delete(self.currentText())

        # Refresh list
        self.populate()

    def rename_node_event(self):
        """
        Will open a user input to rename corresponding blend shape node
        """
        # Get user name input
        title = 'Rename blendShape'
        name, ok = QtGui.QInputDialog.getText(self, title,
                                              'Name',
                                              QtGui.QLineEdit.Normal,
                                              self.currentText())

        # Check results
        if not (name and ok):
            return

        # Check that the name is not currently used
        if cmds.objExists(name):
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'This name already exists:\n"%s"' % name)
            cmds.select(name)
            return

        # Rename the node
        cmds.rename(self.currentText(), name)

        # Refresh list
        self.populate(name)

    def create_new_event(self):
        """
        Create New empty blendShape node on currently selected mesh
        """
        # Get selection
        sel = cmds.ls(sl=True)
        if not sel:
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'Please select a mesh object first.')
            return
        node = api.Node(sel[0])

        # Check selection type
        shape = node.shape()
        if not (shape and shape.has_fn(OpenMaya.MFn.kMesh)):
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'Selected object is not a mesh:\n"%s"' % node)
            return

        # Check that there isn't any blend shape node in the history yet.
        if Commands.mesh_input_blend_shape(shape):
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'Selected object:\n"%s"\nalready seems to have a blendShape.' % node)
            return

        # Get new name
        title = 'Create blendShape'
        name, ok = QtGui.QInputDialog.getText(self, title,
                                              'Name',
                                              QtGui.QLineEdit.Normal,
                                              self.currentText())

        # Check results
        if not (name and ok):
            return

        # Create blend shape
        cmds.deformer(sel[0], type='blendShape', frontOfChain=True, name=name)

        # refresh window
        self.populate(name)


class TemplateComboBox(CustomComboBox):
    """
    Json template combo box selector.
    """
    def __init__(self, window, *args, **kwargs):
        ui.CallbackComboBox.__init__(self, *args, **kwargs)

        self._window = window

        path = os.path.dirname(__file__)
        self._folder_path = os.path.join(path, 'resources', 'templates')
        self._user_folder_path = Commands.project_scenes_folder()

        self._file_paths = list()

    def clear(self, *args, **kwargs):
        """
        Clear the combo box content
        """
        self._file_paths = list()
        return ui.CallbackComboBox.clear(self, *args, **kwargs)

    @staticmethod
    def _files_by_ext(folder, ext='json'):
        """
        Return the lost of json files present un the specified folder

        :param str folder: folder path
        :return type: list
        """
        files = list()

        if not os.path.exists(folder):
            return  files

        for file in os.listdir(folder):
            if file.endswith('.{}'.format(ext)):
                files.append(file)
        files.sort()

        return files

    def _get_default_files(self):
        """
        Will return the list of json file present in the default template folder

        :return type: list
        """
        files = list()
        for ext in data.TemplateData.__VALID_EXTENSIONS__:
            files.extend(self._files_by_ext(self._folder_path, ext))

        files.sort()
        return files

    def populate(self, active_name=None):
        """
        Will populate the combo box with existing template choice.
        """
        active_index = 0
        if not active_name:
            active_name = self.currentText()

        # Reset
        self.clear()

        # Add default json template files
        for file in self._get_default_files():
            file_path = os.path.join(self._folder_path, file)
            self._file_paths.append(file_path)
            for ext in data.TemplateData.__VALID_EXTENSIONS__:
                file = re.sub('.{}'.format(ext), '', file)
            self.addItem(file)

            # Check for default active shape
            if file == active_name:
                active_index = len(self._file_paths) - 1

        # Add user template files
        user_templates = data.UserTemplates.templates()
        user_templates.sort()
        for name in user_templates:
            file_path = data.UserTemplates.path(name)
            self._file_paths.append(file_path)
            self.addItem(name)

            # Check for default active shape
            if name == active_name:
                active_index = len(self._file_paths) - 1

        # Add User import option
        self.addItem('Import...')

        # Set active index
        self.setCurrentIndex(active_index)

    def current_file_path(self):
        """
        Return the file path for the currently selected template

        :return type: str
        """
        # Get selected template
        index = self.currentIndex()

        # Return none if last item (Import...) is selected
        if index == self.count() -1:
            return None

        # Get template file
        file_path = self._file_paths[index]

        return file_path

    def index_change_event(self, index):
        # Run default event on normal call
        if not self.currentText() == 'Import...':
            return ui.CallbackComboBox.index_change_event(self, index=index)

        # Import new user template ca
        self.import_user_template()

    def import_user_template(self):
        """
        Event called to create a new user custom template
        """
        # Get user name input
        title = 'Import...'
        directory = Commands.project_scenes_folder()
        file_filter = 'Maya (*.ma *.mb *.fbx)'
        selected_filter = 'fbx'
        file_path, selected_filter = QtGui.QFileDialog.getOpenFileName(self, title, directory, file_filter, selected_filter)

        if not file_path:
            return

        # Add file to user template list
        data.UserTemplates.add(file_path)

        # Refresh and select new template
        name = data.UserTemplates.name_from_file(file_path)
        self.populate(active_name=name)


class TargetTableView(QtGui.QTableView):
    """
    Target display TableView widget
    """
    def __init__(self,
                 parent=None,
                 blend_shape_data=None,
                 dock_widget=None):
        QtGui.QTableView.__init__(self, parent)

        self.dock_widget = dock_widget

        progress_bar = ui.ProgressBarWindow(parent=self)
        self.commands = Commands(None,
                                 progress_bar=progress_bar)
        self.blend_shape_data = None
        self.set_blend_shape_data(blend_shape_data)
        self._current_target_data = None

        # Context menu support
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.context_menu)

        # Selection behaviour
        self.setAlternatingRowColors(True)
        self.setSelectionBehavior(self.SelectRows)
        self.setSelectionMode(self.NoSelection)
        self.setFocusPolicy(QtCore.Qt.NoFocus)

        # Column size and behaviours
        self.setSortingEnabled(True)
        self.verticalHeader().hide()

        self.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Fixed)
        self.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
        self.horizontalHeader().setStretchLastSection(True)

        # Item data
        self.current_item = None
        self._current_target_data = None
        self._selected_items = list()

        # Click information
        self._mouse_button = None
        self._mouse_pos = None
        self._mouse_time = None
        self._mouse_drag = False
        self.__last_item = None
        self.__last_click_time = time.time()

        # Update dependencies
        scene.TEMPLATE_REFERENCE.commands = self.commands

    def mousePressEvent(self, event):
        """
        Overwrite default event to store mouse information
        """
        # Refresh window if blendShape node is not valid any more
        if self.blend_shape_data and not self.blend_shape_data.node().is_valid():
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'Current blend shape is invalid:\n"%s"\nWindow will refresh' % self.dock_widget.bld_shp_selector.currentText())
            return self.dock_widget.refresh()

        # Update mouse information
        self._mouse_button = event.button()
        if event.button() == QtCore.Qt.LeftButton:
            self._mouse_time = time.time()
            self._mouse_pos = event.pos()

        # store currently clicked elements
        self.current_item = self.indexAt(event.pos())
        self._current_target_data = None
        if self.current_item.isValid ():
            self._current_target_data = self.current_item.model().data(self.current_item, models.TargetsTableModel.__DATA_ROLE__)

        return QtGui.QTableView.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        """
        Overwrite default event to allow driving the shape value on a left button mouse drag.
        """
        # Stop here on none valid move event
        if not (self._mouse_time and (time.time() - self._mouse_time) > 0.25):
            return QtGui.QTableView.mouseMoveEvent(self, event)

        # Set cursor shape
        self.setCursor(QtCore.Qt.SizeHorCursor)

        # Get x position delta
        delta = (event.x() - self._mouse_pos.x()) / (1. * self.columnWidth(1))

        # Update shape weight value
        self.drive_target(delta)

        # Update event position information with static Y
        if self._mouse_pos:
            event = QtGui.QMouseEvent(event.type(),
                                      QtCore.QPoint(event.x(), self._mouse_pos.y()),
                                      event.button(),
                                      event.buttons(),
                                      event.modifiers())

        # Update mouse position information
        self._mouse_pos = event.pos()
        self._mouse_drag = True

        # Return default behaviour
        return QtGui.QTableView.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        """
        Overwrite default event to reset mouse information
        """
        # Reset cursor shape
        self.setCursor(QtCore.Qt.ArrowCursor)

        # Reset mouse information
        self._mouse_time = None

        # Calling click event here instead
        # (releasing the mouse out of the clicked cell won't fire the event, was problematic with drag feature)
        self.click_event()

        return QtGui.QTableView.mouseReleaseEvent(self, event)

    def refresh_row(self, index):
        """
        Refresh display for the specified row index
        """
        # Define row boundaries
        start_index = self.model().index(index, 0)
        end_index = self.model().index(index, self.model().columnCount())

        # Refresh row display
        self.dataChanged(start_index, end_index)

    def refresh_current_row(self):
        """
        Will refresh the current row data
        """
        if not self.current_item:
            return

        self.refresh_row(self.current_item.row())

    # def refresh_selected_rows(self):
    #     """
    #     Will refresh selected row (on data change events)
    #     """
    #     # Get selected row indexes
    #     selected_rows = list()
    #     for cell in self.selectedIndexes():
    #         if cell.row() in selected_rows:
    #             continue
    #         selected_rows.append(cell.row())
    #
    #     for row in selected_rows:
    #         # Get Start and end cell for specified row
    #         start_index = self.model().index(row, 0)
    #         end_index = self.model().index(row, self.model().columnCount())
    #
    #         # Refresh row display
    #         self.dataChanged(start_index, end_index)

    def set_blend_shape_data(self, data):
        """
        Will set the current blend shape data to be displayed

        :param data.BlendShapeData data: blend shape data
        """
        self.blend_shape_data = data
        if data:
            self.commands.blend_shape_node = data.name
            self.commands.template_blend_shape_nodes = data.template_blend_nodes()
        else:
            self.commands.blend_shape_node = None
            self.commands.template_blend_shape_nodes = None

    def _set_target_weight(self, target_data, weight):
        """
        Set the current target weight value

        :param TargetData target_data: target data
        :param float weight: target weight
        """
        # Set target weight
        self.commands.set_target_weight(target_data.index, weight)
        self.commands.set_template_alias_weight(target_data.name, weight)

        # Set all other scene blend shapes
        if self.dock_widget and self.dock_widget.drive_option.isChecked():
            self.commands.scene_set_target_weight(target_data.name, weight)

    def _set_current_target_weight(self, weight):
        """
        Set the current target weight value

        :param float weight: target weight
        """
        self._set_target_weight(self._current_target_data, weight)

    def click_event(self, *args):
        """
        Called when a cell is clicked
        """
        # Skip if drag event
        if self._mouse_drag:
            self._mouse_drag = False
            return

        # Skip on right click
        if self._mouse_button == QtCore.Qt.RightButton:
            return

        # Skip when no blend shape is set
        if not (self.blend_shape_data and self.blend_shape_data.node()):
            return

        # Skip when empty space is clicked
        if not self._current_target_data:
            return

        # Double click timer override (default double click event timer was too long...)
        if (self.currentIndex() == self.__last_item) and (time.time() - self.__last_click_time < 0.25):
            return self.double_click_event()

        # Update click information
        self.__last_click_time = time.time()
        self.__last_item = self.currentIndex()

        # Aboard if missing shape (no index)
        weight = 1.0
        if self._current_target_data.index is None:
            if self.commands.template_weight(self._current_target_data.name):
                weight = 0.0

        elif self.blend_shape_data.node().index_weight(self._current_target_data.index):
            weight = 0.0

        # Reset all target weights if not control modifier
        modifiers = QtGui.QApplication.keyboardModifiers()
        if not modifiers == QtCore.Qt.ControlModifier:
            self.commands.reset_all_targets()
            if self.dock_widget and self.dock_widget.drive_option.isChecked():
                self.commands.scene_reset_all_targets()

        # Update fake "selection" (since selection mode is disabled)
        if modifiers == QtCore.Qt.ControlModifier:
            if self.current_item in self._selected_items:
                self._selected_items.remove(self.current_item)
            else:
                self._selected_items.append(self.current_item)
        else:
            self._selected_items = [self.current_item]

        # Toggle shape
        self._set_current_target_weight(weight)
        self.refresh_current_row()

    def double_click_event(self, *args):
        """
        Called when a cell is double clicked
        """
        if not self.blend_shape_data:
            return

        # Update timer
        self.__last_click_time = time.time()

        # Get selected item target logical index
        self._current_target_data = self.current_item.model().data(self.current_item, models.TargetsTableModel.__DATA_ROLE__)

        # Apply current edited target
        if 'editing' in self._current_target_data.status:
            self.apply_edit_changes_event()

        # Create new target
        elif self._current_target_data.status == 'missing':
            self.create_missing_target_event()

        # Edit target
        else:
            self.edit_target_event()

        # Force shape activation
        self.commands.set_target_weight(self._current_target_data.index, 1.0)
        self.commands.set_template_alias_weight(self._current_target_data.name, 1.0)

    def _missing_targets_dict(self):
        """
        Return the filtered dictionary of the missing targets

        :return: dict
        """
        missing_targets = dict()

        # Parse the missing targets
        for target in self.blend_shape_data.missing_targets:
            # Define key based on target name
            key = target.name[0].upper()

            # Update dictionary
            if key in missing_targets:
                missing_targets[key].append(target)
            else:
                missing_targets[key] = [target]

        return missing_targets

    def _get_assign_to_sub_menu(self):
        """
        Will return a sub menu used by the context menu listing missing shape, used to renamed clicked shape to.

        :return type: QtGui.QMenu
        """
        # Get missing target data
        missing_data = self._missing_targets_dict()
        if not missing_data:
            return

        # Init assign to menu
        menu = QtGui.QMenu('Assign to...')
        menu.setIcon(QtGui.QIcon(Icon.RIGHT))
        menu.__stored_actions = list()

        # Parse missing data
        sorted_keys = missing_data.keys()
        sorted_keys.sort()
        for key in sorted_keys:
            # Init sub menu
            sub_menu = QtGui.QMenu(key)

            # Parse key targets
            for target in missing_data[key]:
                # Init action item
                action = ui.CallbackMenuAction(target.name,
                                                icon=None,
                                                callback=self.assign_to_event,
                                                current_data=self._current_target_data,
                                                target_data=target)

                # Store item (or it won't display for some reason...)
                menu.__stored_actions.append(action)

                # Add to sub menu
                sub_menu.addAction(action)

            # Add sub menu to menu
            menu.addMenu(sub_menu)

        return menu

    def context_menu(self):
        """
        Specific context menu called when a cell is right clicked.
        """
        # Get selected item target logical index
        self._current_target_data = self.current_item.model().data(self.current_item, models.TargetsTableModel.__DATA_ROLE__)
        status = self._current_target_data.status

        # Init context menu
        menu = QtGui.QMenu()

        # Missing shape case
        if status == 'missing':
            create_action = QtGui.QAction(QtGui.QIcon(Icon.ADD), "Create", None)
            create_action.setIconVisibleInMenu(True)
            create_action.triggered.connect(self.create_missing_target_event)
            menu.addAction(create_action)

        # Editing case
        elif 'editing' in status:
            apply_edit_action = QtGui.QAction(QtGui.QIcon(Icon.CONFIRM), "Apply", None)
            apply_edit_action.setIconVisibleInMenu(True)
            apply_edit_action.triggered.connect(self.apply_edit_changes_event)
            menu.addAction(apply_edit_action)

            cancel_edit_action = QtGui.QAction(QtGui.QIcon(Icon.CANCEL), "Cancel", None)
            cancel_edit_action.setIconVisibleInMenu(True)
            cancel_edit_action.triggered.connect(self.cancel_edit_changes_event)
            if not 'missing' in status:
                if not cmds.objExists(self._current_target_data.name + self.commands.__BACKUP_SUFFIX__):
                    cancel_edit_action.setDisabled(True)
            menu.addAction(cancel_edit_action)

            select_action = QtGui.QAction(QtGui.QIcon(Icon.SELECT), "Select", None)
            select_action.setIconVisibleInMenu(True)
            select_action.triggered.connect(self.select_target_event)
            menu.addAction(select_action)

        # Existing target case
        else:
            edit_action = QtGui.QAction(QtGui.QIcon(Icon.EDIT), "Edit", None)
            edit_action.setIconVisibleInMenu(True)
            edit_action.triggered.connect(self.edit_target_event)
            menu.addAction(edit_action)

        # Common
        menu.addSeparator()

        # Rename existing target
        if not 'missing' in status:
            # Rename target option
            rename_action = QtGui.QAction(QtGui.QIcon(Icon.RENAME), "Rename...", None)
            rename_action.setIconVisibleInMenu(True)
            rename_action.triggered.connect(self.rename_target_event)
            menu.addAction(rename_action)

            # Assign sub menu
            assign_menu = self._get_assign_to_sub_menu()
            if assign_menu:
                menu.addMenu(assign_menu)

            # Remove target options
            remove_action = QtGui.QAction(QtGui.QIcon(Icon.TRASHBIN), "Remove", None)
            remove_action.setIconVisibleInMenu(True)
            remove_action.triggered.connect(self.remove_target_event)
            menu.addAction(remove_action)

        # Selection based options
        sel = cmds.ls(sl=True)
        if sel and sel[0] != self._current_target_data.name:
            menu.addSeparator()

            apply_selected_action = QtGui.QAction(QtGui.QIcon(Icon.WARNING), "Apply from selection", None)
            apply_selected_action.setIconVisibleInMenu(True)
            apply_selected_action.triggered.connect(self.apply_selected_event)
            menu.addAction(apply_selected_action)

        # exec the menu and map to position
        menu.exec_(QtGui.QCursor.pos())

    def _confirm_creation(self, target_name):
        """
        Will confirm that user want to replace existing shape if any.

        :param str target_name: target name
        :return type: bool
        """
        # Doesn't exists case
        if not cmds.objExists(target_name):
            return True

        # Open confirmation
        message = 'The following existing mesh will be replaced\n"%s"\n Continue?' % target_name
        reply = QtGui.QMessageBox.question(self,
                                           'Replacing mesh...',
                                           message,
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return False

        cmds.delete(cmds.ls(target_name))
        return True

    def _has_unique_mesh(self, target_name):
        """
        Will check that the specified name is unique in the scene (detecting multiple objects issues)

        :param str target_name: name of the target to test
        :return type: bool
        """
        target_mesh = cmds.ls(target_name)
        if len(target_mesh) > 1:
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'More than one object match name:\n"%s"' % target_name)
            cmds.select(target_mesh)
            return False
        return True

    def create_missing_target_event(self):
        """
        Generate new shape from neutral shape
        """
        target_name = self._current_target_data.name

        # Confirm replacement
        if not self._confirm_creation(target_name):
            return

        # Create new mesh for target
        target_mesh = self.commands.generate_neutral_shape(target_name,
                                                           auto_move=self.dock_widget.place_option.isChecked())
        cmds.select(target_mesh)

        # Update display status
        self._current_target_data.status = 'missing_editing'

        # Force display refresh
        self.refresh_current_row()

    def edit_target_event(self):
        """
        Generate target mesh for tweaking
        """
        target_name = self._current_target_data.name

        # Confirm replacement
        if not self._confirm_creation(target_name):
            return

        # Create backup mesh
        target_index = self._current_target_data.index
        if self.dock_widget and self.dock_widget.backup_option.isChecked():
            self.commands._edit_target_backup(target_index)

        # Create target mesh
        if self.dock_widget:
            target_mesh = self.commands.edit_target(target_index,
                                                    connect=self.dock_widget.live_edit_option.isChecked(),
                                                    auto_move=self.dock_widget.place_option.isChecked())
        else:
            target_mesh = self.commands.edit_target(target_index,
                                                    auto_move=self.dock_widget.place_option.isChecked())
        cmds.select(target_mesh)

        # Change status to editing
        self._current_target_data.update_status()

        # Force display refresh
        self.refresh_current_row()

    def apply_edit_changes_event(self):
        """
        Apply Target edition changes to blend shape
        """
        if not self._has_unique_mesh(self._current_target_data.name):
            return

        # Target shape exists case
        if cmds.objExists(self._current_target_data.name):
            # Update index for new shape
            if self._current_target_data.index is None:
                self._current_target_data.index = self.blend_shape_data.new_index()

            # Connect mesh to input target attribute
            self.commands.apply_target(self._current_target_data.name, self._current_target_data.index)

            # Delete mesh
            cmds.delete(self._current_target_data.name)

            # Delete backup mesh
            backup_mesh = self._current_target_data.name + self.commands.__BACKUP_SUFFIX__
            if cmds.objExists(backup_mesh):
                cmds.delete(backup_mesh)

        # Target shape not found case
        else:
            # Open warning window
            QtGui.QMessageBox.warning(self,
                                      "Warning",
                                      "No shape found for:\n%s" % self._current_target_data.name)
            return

        self._current_target_data.update_status()

        # Force display refresh
        self.refresh_current_row()

    def cancel_edit_changes_event(self):
        """
        Cancel Target edition and forfeit changes
        """
        target_name = self._current_target_data.name

        # Open confirmation
        message = 'Cancel changes to "%s" ?\n They will be lost.' % target_name
        reply = QtGui.QMessageBox.question(self,
                                           'Cancel',
                                           message,
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return

        # Delete target mesh
        if cmds.objExists(target_name):
            if not self._has_unique_mesh(target_name):
                return
            cmds.delete(target_name)

        # Apply backup mesh
        backup_mesh = target_name + self.commands.__BACKUP_SUFFIX__
        if cmds.objExists(backup_mesh):
            self.commands.apply_target(backup_mesh, self._current_target_data.index)
            cmds.delete(backup_mesh)

        # Update target status
        self._current_target_data.update_status()

        # Force display refresh
        self.refresh_current_row()

    def apply_selected_event(self):
        """
        Apply currently selected mesh as target
        """
        target_name = self._current_target_data.name

        # Get current selection
        sel = cmds.ls(sl=True)
        if not sel:
            return

        # Define message
        message = 'Override target "%s" with currently selected mesh ?' % target_name
        if self._current_target_data.status == 'editing':
            message += '\n Current edit shape will be lost.'

        # Open confirmation
        reply = QtGui.QMessageBox.question(self,
                                           'Override',
                                           message,
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return

        # Update target index (for new target)
        if self._current_target_data.index is None:
            self._current_target_data.index = self.commands.target_next_index()

        # Delete mesh
        if cmds.objExists(target_name) and self._has_unique_mesh(target_name):
            cmds.delete(target_name)

        # Delete backup mesh
        backup_mesh = self._current_target_data.name + self.commands.__BACKUP_SUFFIX__
        if cmds.objExists(backup_mesh):
            cmds.delete(backup_mesh)

        # Apply mesh as target
        self.commands.apply_target(sel[0],
                                   self._current_target_data.index,
                                   target_alias=target_name)

        # Update target status
        self._current_target_data.update_status()

        # Force display refresh
        self.refresh_current_row()

    def _rename_target(self, target_data, name):
        """
        Will rename the specified target data instance with the specified name

        :param data.TargetData target_data: Target data instance to rename
        :param str name: New name to assign
        """
        if target_data.name == name:
            return

        # Remove existing target (missing) from data
        self.blend_shape_data.remove_target(name)

        # Rename target
        self.commands.rename_target(target_data.name, name)

        # Disable model
        self.model().beginResetModel()

        # Update target data information
        old_name = target_data.name
        target_data.name = name
        target_data.update_status()

        # Add old name as missing shape if present in template
        if self.blend_shape_data and old_name in self.blend_shape_data.template_aliases():
            self.blend_shape_data.add_target(old_name)

        # Enable model
        self.model().endResetModel()

    def rename_target_event(self):
        """
        Will open a user input and rename the selected target.
        """
        target_name = self._current_target_data.name

        # Prevent renaming of missing shapes
        if 'missing' in self._current_target_data.status:
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'You can not rename a missing target:\n%s' % target_name)
            return

        # Get user name input
        title = 'Rename target'
        name, ok = QtGui.QInputDialog.getText(self,
                                              title,
                                              'Name',
                                              QtGui.QLineEdit.Normal,
                                              target_name)

        # Check results
        if not (name and ok):
            return

        # Same name, nothing to do
        if name == target_name:
            return

        # Check that alias is not used
        aliases = self.blend_shape_data.aliases()
        if name in aliases:
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'This target alias is already used:\n%s' % name)
            return

        # Rename target with data edits
        self._rename_target(self._current_target_data, name)

    def remove_target_event(self):
        """
        Remove the currently selected target item from the blend shape node.
        """
        # Remove target from blend shape node
        self.commands.remove_target(self._current_target_data.index)

        # Disable model
        self.model().beginResetModel()

        # Remove target from data
        self.blend_shape_data.remove_target(self._current_target_data)

        # Enable model
        self.model().endResetModel()

    def select_target_event(self):
        """
        Will select any existing object with the same name as the currently selected target
        """
        target_name = self._current_target_data.name
        mesh = cmds.ls(target_name)
        if not mesh:
            return
        cmds.select(mesh)

    def assign_to_event(self, *args, **kwargs):
        """
        Method called on the right click assign to event
        """
        # Filter input
        current_data = kwargs.get('current_data', None)
        target_data = kwargs.get('target_data', None)

        self._rename_target(current_data, target_data.name)
        del target_data

    def drive_target(self, delta):
        """
        Drive the target with the specified delta increment value
        (Called on mouseMoveEvent while left clicked)

        :param float delta: value to increment the target weight value by.
        """
        # Return on invalid target
        if not self._current_target_data or 'missing' in self._current_target_data.status:
            return

        # Get current target weight
        weight = self.blend_shape_data.node().index_weight(self._current_target_data.index)

        # Add and clamp delta
        new_weight = max(min(weight + delta, 1), 0)
        if new_weight == weight:
            return

        # Stop here on non control modifier (single case)
        modifiers = QtGui.QApplication.keyboardModifiers()
        if not modifiers == QtCore.Qt.ControlModifier:
            # Set current target weight
            self._set_current_target_weight(new_weight)

            # Refresh current row display
            self.refresh_current_row()
            return

        # Update selection list
        if not self.current_item in self._selected_items:
            self._selected_items.append(self.current_item)

        # Drive fake multi selection
        for target_item in self._selected_items:
            # Set weight
            target_data = target_item.model().data(target_item, models.TargetsTableModel.__DATA_ROLE__)
            self._set_target_weight(target_data, new_weight)

            # Refresh row display
            self.refresh_row(target_item.row())


class MainDockWindow(QtGui.QDockWidget):
    """
    Main window
    """
    __TITLE__ = 'fstk Target Manager'
    __OBJ_NAME__ = '_'.join(__TITLE__.split(' ')).lower()

    __DEFAULT_WIDTH__ = 300
    __DEFAULT_HEIGHT__ = 600

    def __init__(self, parent=None):
        QtGui.QDockWidget.__init__(self, parent)
        self.parent = parent

        self._refreshing = False

        # Setup ui
        self.setup()
        self.refresh()
        self.center_on_screen()

    def center_on_screen(self):
        """
        Position window on center screen
        """
        if not self.isFloating():
            return

        desktop_rect = QtGui.QDesktopWidget().screenGeometry(1)
        center_pt = desktop_rect.center()

        x_position = center_pt.x() - (.5 * self.width())
        y_position = center_pt.y() - (.5 * self.height())

        self.move(x_position, y_position)

    def setup(self):
        """
        Setup interface
        """
        # Main window setting
        self.setObjectName(self.__OBJ_NAME__)
        self.setWindowTitle(self.__TITLE__)
        self.resize(self.__DEFAULT_WIDTH__, self.__DEFAULT_HEIGHT__)

        self.setAllowedAreas(QtCore.Qt.AllDockWidgetAreas)
        self.setFeatures(QtGui.QDockWidget.AllDockWidgetFeatures)

        # Add to app main window
        if self.parent:
            self.setFloating(True)
            self.parent.addDockWidget(QtCore.Qt.RightDockWidgetArea, self)

            # # Tabify with other widget (does not seem to work properly)
            # for dock in self.parent.findChildren(QtGui.QDockWidget):
            #     # Skip element if not docked in the right area
            #     if self.parent.dockWidgetArea(dock) != QtCore.Qt.RightDockWidgetArea:
            #         continue
            #
            #     # Tabify to avoid the splitting of the dock area
            #     self.parent.tabifyDockWidget(dock, self)
            #     break

        # Add window widget for menu bar support
        self._window = QtGui.QMainWindow()
        self._window.setParent(self)

        # Add main widget and vertical layout
        self.main_widget = QtGui.QWidget(self._window)
        self.main_vertical_layout = QtGui.QVBoxLayout(self.main_widget)

        # Add window fields
        self.add_blend_selector()
        self.add_template_selector()
        self.add_target_group()
        self.add_top_menu()

        # Add main widget to window
        self._window.setCentralWidget(self.main_widget)
        self.setWidget(self._window)

    def refresh(self):
        """
        Refresh the window
        """
        # Disable table view while data refresh
        self.target_list.setDisabled(True)

        # Populate selectors
        self._refreshing = True
        self.bld_shp_selector.populate()
        self.template_selector.populate()
        self._refreshing = False

        # Update data
        self.blend_shape_event()

        # Re-enable table view
        self.target_list.setEnabled(True)

        # Set default sort column
        self.target_list.sortByColumn(1, QtCore.Qt.AscendingOrder)

    def add_blend_selector(self):
        """
        Blend shape node selector group box
        """
        # Create group box
        group_box = QtGui.QGroupBox(self)
        group_box.setTitle('BlendShape')

        # Add main layout
        layout = QtGui.QHBoxLayout(group_box)

        self.bld_shp_selector = BlendShapeComboBox(callback=self.blend_shape_event)
        layout.addWidget(self.bld_shp_selector)

        # Add refresh buttons
        refresh_button = ui.CallbackButton(callback=self.refresh,
                                            tool_tip='Refresh blendShape list')
        refresh_button.setIcon(QtGui.QIcon(Icon.REFRESH))
        refresh_button.setFixedWidth(20)
        refresh_button.setFixedHeight(20)
        layout.addWidget(refresh_button)

        # Add to main layout
        self.main_vertical_layout.addWidget(group_box)

    def add_template_selector(self):
        """
        Add the template selector group
        """
        # Create group box
        group_box = QtGui.QGroupBox(self)
        group_box.setTitle('Template')

        # Add main layout
        layout = QtGui.QHBoxLayout(group_box)

        # Add toggle check box
        self.template_check_box = ui.CallbackCheckBox(callback=self.template_check_box_event,
                                                       checked=False,
                                                       tool_tip='Toggle template use')
        self.template_check_box.setFixedWidth(15)
        self.template_check_box.setFixedHeight(15)

        layout.addWidget(self.template_check_box)

        # Add Selector
        self.template_selector = TemplateComboBox(window=self, callback=self.blend_shape_event)
        layout.addWidget(self.template_selector)

        # Add to main layout
        self.main_vertical_layout.addWidget(group_box)

    def add_target_group(self):
        """
        Add Target table view  widget to the window
        """
        # Create group box
        group_box = QtGui.QGroupBox()
        group_box.setTitle('Targets')

        # Add layout
        layout = QtGui.QVBoxLayout(group_box)

        # Add filter lineEdit
        target_filter = QtGui.QLineEdit(self)
        # target_filter.setPlaceholderText('Name filter')
        layout.addWidget(target_filter)

        # Add TableView
        self.target_list = TargetTableView(self, dock_widget=self)
        layout.addWidget(self.target_list)

        # Define proxy model
        self._proxyModel = QtGui.QSortFilterProxyModel(self)
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._proxyModel.setSortRole(models.TargetsTableModel.__SORT_ROLE__)
        self._proxyModel.setSortCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._proxyModel.setFilterRole(models.TargetsTableModel.__FILTER_ROLE__)
        self._proxyModel.setFilterKeyColumn(1)

        self.target_list.setModel(self._proxyModel)

        # Connect filter event
        self.connect(target_filter, QtCore.SIGNAL("textChanged(QString)"), self._proxyModel.setFilterRegExp)

        # Add extra buttons
        add_target_button = ui.CallbackButton(callback=self.add_target_event)
        add_target_button.setText('Add...')

        layout.addWidget(add_target_button)

        # Add to main layout 
        self.main_vertical_layout.addWidget(group_box)

    def add_top_menu(self):
        """
        Add the window top menu
        """
        # Get menu bar
        menu_bar = self._window.menuBar()

        # Add File menu
        file_menu = menu_bar.addMenu("&File")
        file_menu.setTearOffEnabled(True)

        create_action = QtGui.QAction("&Create blendShape...",
                                      self,
                                      statusTip="Create a new blendShape on the selected mesh",
                                      triggered=self.bld_shp_selector.create_new_event)
        file_menu.addAction(create_action)

        self.delete_action = QtGui.QAction("&Delete blendShape...",
                                           self,
                                           statusTip="The currently active blend shape node",
                                           triggered=self.bld_shp_selector.delete_node_event)
        file_menu.addAction(self.delete_action)

        file_menu.addSeparator()

        exit_action = QtGui.QAction("&Exit",
                                    self,
                                    statusTip="Close this tool",
                                    triggered=self.close)
        file_menu.addAction(exit_action)

        # Add Edit commands
        edit_menu = menu_bar.addMenu("&Edit")
        edit_menu.setTearOffEnabled(True)

        generate_all_action = QtGui.QAction("&Generate all targets",
                                            self,
                                            statusTip="Generate all the targets of the blendShape",
                                            triggered=self.generate_all_targets)
        edit_menu.addAction(generate_all_action)

        apply_all_action = QtGui.QAction("&Apply all targets",
                                         self,
                                         statusTip="Apply all the targets in edition mode",
                                         triggered=self.apply_all_targets)
        edit_menu.addAction(apply_all_action)

        rename_action = QtGui.QAction("&Search and replace...",
                                      self,
                                      statusTip="Will search and replace all target names...",
                                      triggered=self.search_and_replace_all_targets)
        edit_menu.addAction(rename_action)

        reorder_action = QtGui.QAction("&Reorder targets...",
                                      self,
                                      statusTip="Will reorder the target alphabetically on the blend shape node",
                                      triggered=self.reorder_targets)
        edit_menu.addAction(reorder_action)

        # Add options
        options_menu = menu_bar.addMenu("&Options")
        options_menu.setTearOffEnabled(True)

        self.drive_option = QtGui.QAction("&Drive all blendShapes",
                                          self,
                                          statusTip="Click drive all blendShape together")
        self.drive_option.setCheckable(True)
        self.drive_option.setChecked(True)
        options_menu.addAction(self.drive_option)

        self.live_edit_option = QtGui.QAction("&Live edits",
                                          self,
                                          statusTip="Will connect the target mesh to the blendShape for live feedback")
        self.live_edit_option.setCheckable(True)
        self.live_edit_option.setChecked(True)
        options_menu.addAction(self.live_edit_option)

        self.backup_option = QtGui.QAction("&Backup target on edit",
                                          self,
                                          statusTip="Will create a backup target mesh on edit to enable edit cancel")
        self.backup_option.setCheckable(True)
        self.backup_option.setChecked(True)
        options_menu.addAction(self.backup_option)

        self.place_option = QtGui.QAction("&Auto place target mesh",
                                          self,
                                          statusTip="Will automatically offset/place target edit mesh")
        self.place_option.setCheckable(True)
        self.place_option.setChecked(True)
        options_menu.addAction(self.place_option)

        # Add Help menu
        # menu_bar.addSeparator()
        help_menu = menu_bar.addMenu("&Help")
        help_menu.setTearOffEnabled(True)

        help_action = QtGui.QAction("&Help",
                                    self,
                                    statusTip="Open the documentation in a browser",
                                    triggered=self.help_event)
        help_menu.addAction(help_action)

        about_action = QtGui.QAction("&About",
                                     self,
                                     statusTip="Shows the tool description",
                                     triggered=self.about_event)
        help_menu.addAction(about_action)

    def _set_targets_data(self, data):
        """
        Update the model view with the data

        :param data.BlendShapeData data: target data
        """
        # Update blend shape node information
        self.target_list.set_blend_shape_data(data)

        # Set model
        self._model = models.TargetsTableModel(root=data, view_widget=self.target_list)
        self._proxyModel.reset()
        self._proxyModel.setSourceModel(self._model)

        # Forced here, must be set after data
        self.target_list.setColumnWidth(0, 50)

    def blend_shape_event(self, *args, **kwargs):
        """
        Method called on combo box trigger change event, will update the model view data
        """
        # Skip on first refresh
        if self._refreshing:
            return

        # Reset and abort if blend shape selector is empty
        if not self.bld_shp_selector.count():
            self._set_targets_data(None)
            return

        # Update data with template
        if self.template_check_box.isChecked():
            template = self.template_selector.current_file_path()
        else:
            template = None

        # Get blendShape
        blend_shape = self.bld_shp_selector.currentText()

        # Initialise data
        blend_data = data.BlendShapeData(blend_shape,
                                         template_file=template)

        # Apply data to model view
        self._set_targets_data(blend_data)

    def template_check_box_event(self, value=False):
        """
        Method called when the template toggle check box status changes

        :param bool value: check box status
        """
        current_data = self.target_list.blend_shape_data

        # Update blendShape information
        self.blend_shape_event()

        # Unload reference
        if not current_data:
            return
        if not value:
            current_data._template_data.reference.disable()

    def add_target_event(self, *args, **kwargs):
        """
        Method called when the add target button is pressed
        """
        # Abort if no valid blendShape
        blend_node = self.bld_shp_selector.currentText()
        if not (blend_node and cmds.objExists(blend_node)):
            return

        data = self.target_list.blend_shape_data

        # Get user name input
        title = 'Add target...'
        name, ok = QtGui.QInputDialog.getText(self, title,
                                              'Name',
                                              QtGui.QLineEdit.Normal,
                                              '')

        # Check results
        if not (name and ok):
            return

        # Check that the name is not currently used
        if name in data.targets:
            QtGui.QMessageBox.warning(self,
                                      'Warning',
                                      'This name is already in used:\n"%s"' % name)
            cmds.select(name)
            return

        # Target mesh exists
        if cmds.ls(name):
            # Open confirmation
            message = 'Target mesh found for\n"%s"\nWill use the existing mesh' % name
            reply = QtGui.QMessageBox.question(self,
                                               'Existing mesh...',
                                               message,
                                               QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                               QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.No:
                return

            self.target_list.commands.apply_target(name)
            target_mesh = api.Node(name)

        # Add new target
        else:
            target_mesh = self.target_list.commands.add_target(name)

        # Select target mesh
        cmds.select(target_mesh)

        # self._model.beginInsertRows()
        self._model.beginResetModel()
        self.target_list.blend_shape_data.add_target(name)
        self._model.endResetModel()

    def generate_all_targets(self):
        """
        Generate all target mesh (switching them to edit mode)
        """
        self.target_list.commands.edit_all_targets()
        self.refresh()

    def apply_all_targets(self):
        """
        Apply all targets currently in edit mode.
        """
        self.target_list.commands.apply_all_targets()
        self.refresh()

    def search_and_replace_all_targets(self):
        """
        Will open a input window for pattern and replace string,
        then will processed a search and replace on all the targets.
        """
        # Open Search and replace dialog window
        pattern, replace, ok = ui.SearchAndReplaceDialog.get()
        if not ok:
            return

        # Apply batch renaming
        self.target_list.commands.search_replace_targets(pattern, replace)
        self.refresh()

    def reorder_targets(self):
        """
        Will open confirmation window before reordering all targets on the blend shape node in alphabetical order.
        """
        if not self.bld_shp_selector.currentIndex() >= 0:
            return

        # Open confirmation window
        title = 'Reorder targets'
        message = 'Are you sure you want to reorder all targets on the blend shape node\n"{}" ?'.format(self.bld_shp_selector.currentText())
        reply = QtGui.QMessageBox.question(self,
                                           title,
                                           message,
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.No:
            return False

        # Start process
        self.target_list.commands.reorder_targets()
        self.refresh()

    def help_event(self):
        """
        Will open the help documentation url
        """
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(faceshifttoolkit.__DOCUMENTATION_URL__))

    def about_event(self):
        """
        Open the about window
        """
        message_box = QtGui.QMessageBox()
        message_box.setIconPixmap(QtGui.QPixmap(Icon.APP))
        message_box.setWindowTitle('About the Target Manager')

        description = """
            <p>The <b>Target Manager</b> is part of the <b>faceshift toolkit</b></p>
            <p>The <b>faceshift toolkit</b> is free software:<br>
            you can redistribute it and/or modify it under the terms of the GNU General Public License, either version 3 of the License, or (at your option) any later version.</p>
            <br><br>
            <b>Version</b>: {}<br>
            <b>Author</b>: {}<br>
            <b>Repository</b>: {}
        """.format(faceshifttoolkit.__version__,
                   faceshifttoolkit.__author__,
                   faceshifttoolkit.__url__)
        message_box.setText(description)

        message_box.exec_()
