# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

import re
import os
import time

from maya import cmds

from faceshifttoolkit.shared.handlers import api
from faceshifttoolkit.shared.handlers import scene


class UserTemplates(object):
    """
    Hold the user imported templates file_path list for this sessions
    """
    _DATA = dict()

    @staticmethod
    def name_from_file(file_path):
        """
        Return the file name without extension from a file path

        :param str file_path: file path
        :return type: str
        """
        return file_path.rsplit(os.path.sep, 1)[-1].rsplit('/', 1)[-1].rsplit('.', 1)[0]

    @classmethod
    def templates(cls):
        """
        Return the list of registered user template names

        :return type: list
        """
        return cls._DATA.keys()

    @classmethod
    def path(cls, name):
        """
        Return the path corresponding to the specified user template name

        :param str name: user template name
        :return type: str
        """
        return cls._DATA.get(name, None)

    @classmethod
    def add(cls, file_path):
        """
        Add the specified file path to the user template list

        :param str file_path: template scene file path
        """
        name = cls.name_from_file(file_path)
        cls._DATA[name] = file_path

    @classmethod
    def remove(cls, file_path):
        """
        Remove the specified file path from the user tempalte list

        :param str file_path: file path
        """
        name = cls.name_from_file(file_path)
        if not name in cls._DATA:
            return
        del(cls._DATA[name])


class TemplateData(object):
    """
    Template data handler
    """
    __VALID_EXTENSIONS__ = ['mb', 'ma', 'fbx']

    def __init__(self, file_path=None):
        self._file_path = None
        self.__data = dict()
        self.reference = scene.TEMPLATE_REFERENCE

        self.file = file_path

    @property
    def file(self):
        """
        :return: the template file path
        """
        if self.is_valid():
            return self._file_path
        return None

    @file.setter
    def file(self, file_path):
        """
        Set the file path for this template instance

        :param str file_path: template file path
        """
        self._file_path = file_path
        if not file_path:
            return
        assert self.is_valid(), 'file "%s" not found' % file_path
        self.load_file()

    def is_valid(self):
        """
        Check the validity of the template file path

        :return type: bool
        """
        if not self._file_path:
            return False
        return os.path.exists(self._file_path)

    def file_type(self):
        """
        Return the file extension of the template file (None if invalid)

        :return type: str
        """
        # No file specified
        if not self.is_valid():
            return

        # Get the file extension
        ext = os.path.splitext(self.file)[1]

        # Return if valid extension
        if ext and ext[1:] in self.__VALID_EXTENSIONS__:
            return ext[1:]

    def _load_maya_file(self):
        """
        Load the template file as a maya reference
        """
        assert self.file_type() in self.__VALID_EXTENSIONS__, 'file "{}" is not a supported maya file'.format(self.file)
        self.__data = dict()

        # Init reference file
        self.reference.file = self.file

        # Get blend shape nodes
        blend_shapes = self.reference.ls(type='blendShape')
        if not blend_shapes:
            return self.__data

        self.__data["nodes"] = blend_shapes

        # Convert to BlendShapeNodes
        for i in range(len(blend_shapes)):
            blend_shapes[i] = api.BlendShapeNode(blend_shapes[i])

        # Get the largest blend shape (should hold all the template aliases)
        ref_blend = blend_shapes[0]
        for node in blend_shapes[1:]:
            if not len(node) > len(ref_blend):
                continue
            ref_blend = node

        # Get the target alias list
        aliases = ref_blend.aliases()

        self.__data["targets"] = aliases

    def load_file(self):
        """
        Load the template file
        """
        file_type = self.file_type()
        if not file:
            return

        if file_type in ['ma', 'mb', 'fbx']:
            return self._load_maya_file()

    def targets(self):
        """
        Return the known target list of this template

        :return type: list
        """
        targets = self.__data.get('targets', list())
        targets.sort()
        return targets

    def blend_shape_nodes(self):
        return self.__data.get('nodes', list())

    def set_alias_weight(self, alias, weight=0.0):
        """
        Will set the specified alias weight on all template blend shape nodes

        :param str alias: target alias
        :param float weight: weight
        """
        # Parse blend shape nodes
        for node in self.__data.get('nodes', list()):
            # Skip invalid nodes
            if not isinstance(node, api.BlendShapeNode):
                continue

            # Set target weight
            node.set_alias_weight(alias, weight)


class BaseDataNode(object):
    """
    Base data node, should not be instantiated directly,
    holds common methods shared between BlendShapeData and TargetData
    """
    def __init__(self, name):
        """
        :param name: name of the object
        """
        self._name = None
        self.name = name

    def __str__(self):
        return self.name

    def __eq__(self, other):
        """
        Sets and lists filtering support
        """
        return self.__hash__() == hash(other)

    def __hash__(self):
        return hash(self.name)

    @property
    def name(self):
        """
        Return the name of the associated node
        """
        return self._name

    @name.setter
    def name(self, value):
        """
        Set the name of the associated node

        :param str value: name of the associated node
        """
        self._name = value


class BlendShapeData(BaseDataNode):
    """
    BlendShape Data node, used as the base data node used for the view model
    """
    def __init__(self, name, template_file=None):
        """
        :param str name: name of the blend shape node
        :param str template_file: file to the template file
        """
        BaseDataNode.__init__(self, name)

        if cmds.objExists(name):
            self._node = api.BlendShapeNode(name)
        else:
            self._node = None

        self._targets = list()

        self.__previous_time = None
        self.__temp_target_aliases = list()
        self.__temp_reference_aliases = list()

        self._template_data = TemplateData()
        self.set_template_file(template_file)

    def count(self):
        """
        Return the number of targets

        :return type: int
        """
        return len(self.targets)

    def node(self):
        return self._node

    def is_valid(self):
        return self.node().is_valid()

    @property
    def targets(self):
        """
        Return the list of targets instances (current blend shape and template)

        :return type: list
        """
        return self._targets

    @targets.setter
    def targets(self, target_list):
        """
        Set the list of target instances

        :param list target_list: list if TargetData instances
        """
        assert isinstance(target_list, list), 'wrong type for target_list should be a list'
        self._targets = target_list

    @property
    def missing_targets(self):
        """
        Will return the missing target instance from the template if set.

        :return type: list
        """
        if not self._template_data:
            return

        # Filter targets on missing status
        missing_targets = list()
        for target in self.targets:
            if not 'missing' in target.status:
                continue
            missing_targets.append(target)

        return missing_targets
        
    def set_template_file(self, file_path):
        """
        Will load a json file for reference comparison.

        :param str file_path: template data file path
        """
        self._template_data = TemplateData(file_path)
        self.refresh()

    def template_blend_nodes(self):
        """
        Return the list of template blend shape nodes

        :return type: list
        """
        return self._template_data.blend_shape_nodes()

    def template_aliases(self):
        """
        Return the tarte list for the template

        :return type: list
        """
        return self._template_data.targets()

    def refresh(self):
        """
        Refresh the blendShape data with the blendShape targets and the template targets
        """
        # Get blend shape target aliases
        aliases = self.aliases()

        # Add template target aliases
        template_aliases = self.reference_aliases()
        template_aliases.sort()

        for alias in template_aliases:
            if not alias:
                continue
            if alias in aliases:
                continue
            aliases.append(alias)

        # Process targets
        targets = list()
        for i in range(len(aliases)):
            if not aliases[i]:
                continue
            target = TargetData(aliases[i],
                                index=self._node.alias_index(aliases[i]),
                                blend_data=self)
            target.update_status()
            targets.append(target)

        self.targets = targets

    def aliases(self):
        """
        Return the list a current target aliases for the blendshape

        :return type: list
        """
        if not self._node:
            return list()
        return self._node.aliases()

    def new_index(self):
        """
        Return the next available new index from the blendShape

        :return type: int
        """
        if self._node is None:
            return None
        return len(self._node)

    def reference_aliases(self):
        """
        Return the list of reference target aliases from the selected json file

        :return type: list
        """
        return self._template_data.targets()

    def get_target_status(self, target):
        """
        Return the current target status

        :param str target: target name
        :return type: str
        """
        # Optimization, will only update target list every set given time
        current_time = time.time()
        if not self.__previous_time or (current_time - self.__previous_time) > 1:
            self.__previous_time = current_time
            self.__temp_target_aliases = self.aliases()
            self.__temp_reference_aliases = self.reference_aliases()

        target_aliases = self.__temp_target_aliases
        template_aliases = self.__temp_reference_aliases

        # No template date
        if not template_aliases:
            if target in target_aliases:
                if cmds.objExists(target):
                    return 'editing'
                return 'valid'

        # Template data available
        if target in template_aliases:
            # Blend shape target
            if target in target_aliases:
                if cmds.objExists(target):
                    return 'editing'
                return 'valid'

            # Missing target
            if cmds.objExists(target):
                return 'missing_editing'
            return 'missing'

        # Unknown target case
        elif cmds.objExists(target):
            return 'unknown_editing'
        return 'unknown'

    def target(self, index):
        """
        Return the target data instance for the specified index

        :param int index: index of the target
        :return type: TargetData
        """
        return self.targets[index]

    def child(self, index):
        """
        Return the target data instance for the specified index

        :param int index: index of the target
        :return type: TargetData
        """
        return self.target(index)

    def remove_target(self, name):
        """
        Remove specified target for the target data

        :param str name: target name or TargetData instance
        """
        if name in self._targets:
            self._targets.remove(name)

    def add_target(self, name):
        """
        Add a "missing" target to the blendShape list, allowing for easy create/edition

        :param str name: name of the target
        """
        if name in self._targets:
            return

        # Initialize new target instance
        target = TargetData(name, blend_data=self)
        target.update_index()

        # Add new target to data
        self._targets.append(target)


class TargetData(BaseDataNode):
    """
    Target data used by the blendShape data, contains all the target relevant data.
    """
    # Define name regex
    __NAME_REGEX__ = re.compile(r'([a-zA-Z]+(?:_[C|L|R])*(?:_[0-9]+)*)')

    __STATUS__ = ['valid', 'editing', 'missing', 'missing_editing', 'unknown', 'unknown_editing', ]

    def __init__(self,
                 name,
                 index=None,
                 status=None,
                 blend_data=None,
                 parent=None):
        """
        :param str name: name of the target
        :param int index: blend shape target index
        :param str status: current status of the target
        :param BlendShapeData blend_data: blend shape data instance
        :param TargetData parent: target parent (optional)
        """
        BaseDataNode.__init__(self, name)

        self._index = index
        self._status = None
        self._blend_data = blend_data

        self.status = status

        # In between support
        self._parent = None
        self.set_parent(parent)
        self._children = list()

        # Corrective shape support
        self._components = list()

    def count(self):
        return len(self._children)

    def is_valid(self):
        return self._blend_data.is_valid()

    @classmethod
    def name_is_valid(cls, name):
        """
        Check that the name respects naming convention

        :param str name: name to check
        :return type: bool
        """
        return '_'.join(cls.__NAME_REGEX__.findall(name)) == name

    @property
    def index(self):
        """
        The target attribute index on the blendShape node

        :return type: int
        """
        return self._index

    @index.setter
    def index(self, value):
        self._index = value

    @property
    def status(self):
        """
        Current status of the target (valid, editing...)

        :return type: str
        """
        return self._status

    @status.setter
    def status(self, value):
        """
        Set the status of the shape instance

        :param str value: shape status
        """
        # Check for status if no value specified
        if not value:
            self.update_status()
            return

        # Update status
        assert value in self.__STATUS__, 'Invalid status "%s"' % value
        self._status = value

    def parent(self):
        """
        The parent of the shape for inbetween shapes

        :return type: TargetData
        """
        return self._parent

    def set_parent(self, parent):
        if not parent:
            self._parent = None
            return

        assert isinstance(parent, TargetData), 'Not a valid parent type'
        self._parent = parent
        parent.add_child(self)

    def add_child(self, child):
        """
        Add an inbetween target

        :param TargetData child: child target instance
        """
        assert isinstance(child, TargetData), 'Not a valid child type'

        # Add child to children list
        if child in self._children:
            return
        self._children.append(child)

        # Add self to child parent list
        child.set_parent(self)

    def child(self, index):
        """
        Return the inbetween target at the specified index

        :param int index: index
        :return type: TargetData
        """
        return self._children[index]

    def children(self):
        """
        Return the inbetween target list
        """
        return self._children

    def get_index(self):
        """
        Return the index of this object in the parent child list

        :return type: int
        """
        if not self.parent():
            return None
        return self.parent().children().index(self)

    def update_status(self):
        """
        Will recheck and update status
        """
        if not self._blend_data:
            return
        self.status = self._blend_data.get_target_status(self.name)
        if self.status.count('missing'):
            self.index = None

    def update_index(self):
        """
        Change the target index
        """
        if not self._blend_data:
            self._index = None
            return
        self._index = self._blend_data.node().alias_index(self.name)

    def status_icon(self):
        """
        Return the status icon path

        :return: str
        """
        from .resources.icons import Icon

        if self.status == 'valid':
            return Icon.LIGHT_GREEN
        elif self.status == 'editing':
            return Icon.LIGHT_YELLOW
        elif self.status == 'missing':
            return Icon.LIGHT_RED
        elif self.status == 'missing_editing':
            return Icon.LIGHT_YELLOW_RED
        elif self.status == 'unknown_editing':
            return Icon.LIGHT_YELLOW_GREY
        else:
            return Icon.LIGHT_GREY

    def status_tooltip(self):
        """
        Return the shape status tool tip

        :return type: str
        """
        # Need to add index to force tooltip position refresh ..
        index = self.index
        if index is None:
            index = '#'

        # Return proper tooltip
        if self.status == 'valid':
            return 'Valid target {}'.format(index)
        elif self.status == 'editing':
            return 'Valid target {} in edition mode'.format(index)
        elif self.status == 'missing':
            return 'Missing target {}'.format(index)
        elif self.status == 'missing_editing':
            return 'Missing target {} in edition mode'.format(index)
        elif self.status == 'unknown_editing':
            return 'Unknown target {} in edition mode'.format(index)
        else:
            return 'Unknown target {}'.format(index)

    def filter_components_from_name(self):
        """
        Process the name to determine the parent targets

        :return type: list
        """
        # Find valid name blocks contained in name
        components = self.__NAME_REGEX__.findall(self.name)

        # Remove self from list
        if self.name in components:
            components.remove(self.name)

        components.sort()
        return components

    def is_corrective(self):
        """
        Return if the target is a corrective target or not

        :return type: bool
        """
        if self.filter_components_from_name():
            return True
        return False

    @property
    def weight(self):
        if not self._blend_data or self.index is None:
            return
        return self._blend_data.node().index_weight(self.index)
