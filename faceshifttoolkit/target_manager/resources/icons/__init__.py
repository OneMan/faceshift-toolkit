# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

import os

__THIS_PATH__ = os.path.dirname(__file__)


def _path(name):
    file_name = '%s.png' % name
    return os.path.join(__THIS_PATH__, file_name)


class Icon(object):
    APP = _path('logo')

    EDIT = _path('edit')
    CONFIRM = _path('confirm')
    CANCEL = _path('edit_cancel')
    ADD = _path('edit_add')
    UNDO = _path('undo')
    SELECT = _path('select')
    RENAME = _path('rename')

    TRASHBIN = _path('trashbin')
    REFRESH = _path('refresh')
    WARNING = _path('warning')

    DOWN = _path('down_arrow')
    UP = _path('up_arrow')
    RIGHT = _path('right_arrow')

    LIGHT_GREEN = _path('light_green')
    LIGHT_GREY = _path('light_grey')
    LIGHT_YELLOW = _path('light_yellow')
    LIGHT_RED = _path('light_red')
    LIGHT_BLUE = _path('light_blue')
    LIGHT_BLUE_QUESTION = _path('light_blue_question')

    LIGHT_YELLOW_BLUE_QUESTION = _path('light_yellow_blue_question')
    LIGHT_YELLOW_GREY = _path('light_yellow_grey')
    LIGHT_YELLOW_RED = _path('light_yellow_red')
