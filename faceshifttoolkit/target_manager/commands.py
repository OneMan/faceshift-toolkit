# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import re

from maya import cmds

from faceshifttoolkit.shared.handlers import api
from faceshifttoolkit.shared.handlers import scene


class TargetShader(object):
    """
    New target default shader (clay color)
    """

    __MATERIAL__ = 'TargetDefault_MAT'
    __SG__ = __MATERIAL__.replace('MAT', 'SG')

    def __init__(self):

        # Define custom attribute values
        self.values = {'colorR': 0.62,
                       'colorG': 0.337,
                       'colorB': 0.259,
                       'specularColorR': 0.051,
                       'specularColorG': 0.204,
                       'specularColorB': 0.275,
                       'cosinePower': 5.43}

        # Init nodes
        self._create_material()

    def _create_sg(self):
        """
        Create the proper SG node

        :return: SG node node
        """
        # Return existing SG node
        if cmds.objExists(self.__SG__):
            return self.__SG__

        # Create new SG node
        sg_node = cmds.sets(renderable=True,
                            noSurfaceShader=True,
                            empty=True,
                            name=self.__SG__)

        return sg_node

    def _create_material(self):
        """
        Create the material node

        :return: node name
        """
        # Return existing material
        if cmds.objExists(self.__MATERIAL__):
            return self.__MATERIAL__

        # Create material
        node = cmds.shadingNode('phong', asShader=True, name=self.__MATERIAL__)

        # Set attribute values
        for attr in self.values:
            cmds.setAttr('{}.{}'.format(node, attr), self.values[attr])

        # Connect SG
        sg = self._create_sg()
        cmds.connectAttr('{}.outColor'.format(node), '{}.surfaceShader'.format(sg), f=True)

        return node

    def get_material(self):
        """
        :return: the name of the material node
        """
        return self.__MATERIAL__

    def get_sg(self):
        """
        :return: the name of the related SG node
        """
        return self.__SG__

    def assign_to(self, mesh):
        """
        Assign the shader to the specified object
        :param str mesh: name of the mesh to assign the shader to
        """
        return cmds.sets(mesh, e=True, forceElement=self.get_sg())


class Commands(object):
    """
    Command wrapper from the UI to the software specific commands
    """
    __BACKUP_SUFFIX__ = '_BACKUP'

    def __init__(self, blend_shape_name, grid_width=10, progress_bar=None):
        self._node = None
        self.blend_shape_node = blend_shape_name
        self._template_nodes = list()
        self._grid_width = grid_width
        self.progress_bar = progress_bar

    @property
    def blend_shape_node(self):
        """
        Return the associated blend shape node

        :return type: BlendShapeNode
        """
        return self._node

    @blend_shape_node.setter
    def blend_shape_node(self, name):
        """
        Set the associated blend shape node to the specified name

        :param str name: name of the blendShape
        """
        self._node = None
        if name and cmds.objExists(name):
            self._node = api.BlendShapeNode(name)

    @property
    def template_blend_shape_nodes(self):
        """
        Return the list of the template blend shape nodes

        :return type: list
        """
        return self._template_nodes

    @template_blend_shape_nodes.setter
    def template_blend_shape_nodes(self, nodes):
        """
        Set the list of the template blend shape nodes

        :param list nodes: list of template blend shape nodes
        """
        self._template_nodes = list()
        if not nodes:
            return

        for node in nodes:
            if not isinstance(node, api.BlendShapeNode) and node.is_valid():
                continue
            self._template_nodes.append(node)

    def target_count(self):
        """
        Return targets count number, do not use this to parse targets, use the target_indexes method instead.

        :return type: int
        """
        return len(self.blend_shape_node)

    def target_indexes(self):
        """
        Return the valid used indexes.
        You should use this method to loop over targets instead of the target_count()

        :return type: MIntArray
        """
        return self.blend_shape_node.indexes

    def target_next_index(self):
        """
        Return the next unused index from the blend shape node

        :return: int
        """
        return self.blend_shape_node.next_index()

    def reset_all_targets(self):
        """
        Reset all the targets weight on the associated blend shape and template blend shapes nodes
        """
        self.blend_shape_node.reset_all_targets()
        for node in self.template_blend_shape_nodes:
            node.reset_all_targets()

    def target_weight(self, target):
        """
        Return the target current weight

        :param int/str target: target name to query
        :return type: float
        """
        if not self.blend_shape_node:
            return
        if type(target) is int:
            return self.blend_shape_node.index_weight(target)
        return self.blend_shape_node.alias_weight(target)

    def template_weight(self, target):
        """
        Return the template target current weight

        :param int/str target: target name to query
        :return type: float
        """
        if not self.template_blend_shape_nodes:
            return
        if type(target) is int:
            return self.template_blend_shape_nodes[0].index_weight(target)
        return self.template_blend_shape_nodes[0].alias_weight(target)

    def set_target_weight(self, target_index, weight=0.0):
        """
        Set the target weight

        :param int target_index: target index
        :param float weight: new target weight
        """
        self.blend_shape_node.set_index_weight(target_index, weight)

    def set_template_alias_weight(self, alias, weight=0.0):
        """
        Set the template target weight

        :param str alias: target index
        :param float weight: new target weight
        """
        if not alias:
            return
        for node in self.template_blend_shape_nodes:
            node.set_alias_weight(alias, weight)

    def _get_scene_other_blend_shapes_nodes(self):
        """
        Will return the other non referenced blend shape nodes from the scene

        :return type: list
        """
        blend_nodes = list()
        # Parse blendShapes
        for blend_shape in cmds.ls(type='blendShape'):
            # Skip referenced blend node
            if cmds.referenceQuery(blend_shape, isNodeReferenced=True):
                continue

            # Skip registered blend shape
            if blend_shape == self.blend_shape_node:
                continue

            # Convert to api node
            blend_nodes.append(api.BlendShapeNode(blend_shape))

        return blend_nodes

    def scene_set_target_weight(self, target_name, weight):
        '''
        Will set for all other blend shape in scene the target weight if present

        :param str target_name: name of the target
        :param float weight: weight
        '''
        if type(target_name) == int:
            target_name = self.blend_shape_node.index_alias(target_name)

        for node in self._get_scene_other_blend_shapes_nodes():
            node.set_alias_weight(target_name, weight)

    def scene_reset_all_targets(self):
        """
        Will reset all other targets for other blend shape nodes in the scene.
        """
        for node in self._get_scene_other_blend_shapes_nodes():
            node.reset_all_targets()

    def generate_neutral_shape(self, target_name, auto_move=True):
        """
        Generate a copy of the neutral shape

        :param str target_name: name of the new neutral shape copy
        :return: Node
        """
        mesh = self.blend_shape_node.generate_neutral_shape(target_name)
        if auto_move:
            self.move_from_bounding_box(mesh)
        self.assign_default_shader(mesh)
        return mesh

    def _edit_target_backup(self, target_index, new_name=None, geom_index=0):
        """
        Generate the specified target from the blend shape target data, for edition

        :param int target_index: target index
        :param str new_name: name of the new mesh
        :param int geom_index: geometry index
        :return: Node
        """
        # Define mesh name
        if not new_name:
            new_name = self.blend_shape_node.index_alias(target_index)
        new_name += self.__BACKUP_SUFFIX__

        # Create target mesh
        mesh = self.edit_target(target_index,
                                new_name=new_name,
                                geom_index=geom_index,
                                connect=False,
                                auto_move=False)
        mesh.attr_mplug('visibility').setBool(False)

        return mesh

    def edit_target(self, target_index, new_name=None, geom_index=0, connect=True, auto_move=True):
        """
        Generate the specified target from the blend shape target data, for edition

        :param int target_index: target index
        :param str new_name: name of the new mesh
        :param int geom_index: geometry index
        :return: Node
        """
        # Create target mesh
        mesh = self.blend_shape_node.generate_target_shape_by_index(target_index,
                                                                    new_name=new_name,
                                                                    geom_index=geom_index,
                                                                    connect=connect)
        if auto_move:
            self.move_from_bounding_box(mesh)
        self.assign_default_shader(mesh)
        return mesh

    def edit_all_targets(self, geom_index=0):
        """
        Generate all the targets from the blend shape target data.

        :param int geom_index: geometry index
        :return: list
        """
        if self.progress_bar:
            self.progress_bar.show()
            self.progress_bar.maximum = self.target_count()

        targets = list()
        backups = list()
        for i in self.target_indexes():
            # Update progress bar
            if self.progress_bar:
                self.progress_bar.value += 1

            # Get mesh name
            alias = self.blend_shape_node.index_alias(i)
            if not alias or cmds.objExists(alias):
                continue

            # Edit target
            targets.append(self.edit_target(i, geom_index=geom_index))
            backups.append(self._edit_target_backup(i, geom_index=geom_index))

        return targets, backups

    def apply_target(self, mesh_name, target_index=None, target_alias=None, weight=1.0, geom_index=0):
        """
        Apply the target offset to the blend shape target data

        :param str mesh_name: target mesh name
        :param int target_index: target index
        :param str target_alias: target name (for new targets, different from mesh_name)
        :param float weight: weight of the target (for in-between shapes)
        :param int geom_index: geometry index
        """
        # Update target data
        if target_index in self.target_indexes():
            self.blend_shape_node.update_target_offset_data(target_index,
                                                            mesh=mesh_name,
                                                            geom_index=geom_index)
            return

        # Check index
        if target_index is None:
            target_index = self.blend_shape_node.next_index()

        # Add new target to blend shape
        self.blend_shape_node.add_target(mesh_name,
                                         target_alias=target_alias,
                                         weight=weight,
                                         geom_index=0)

    def apply_all_targets(self, geom_index=0):
        """
        Apply all targets currently in edit mode

        :param int geom_index: geometry index
        """
        # Reset progress bar
        if self.progress_bar:
            self.progress_bar.maximum = self.target_count()
            self.progress_bar.show()

        # Parse targets
        for i in self.target_indexes():
            # Update progress bar
            if self.progress_bar:
                self.progress_bar.value += 1

            # Get target mesh
            alias = self.blend_shape_node.index_alias(i)
            if not cmds.objExists(alias):
                continue

            # Apply targets edits
            self.apply_target(alias, i, geom_index=geom_index)

            # Delete target mesh
            cmds.delete(alias)

            # Delete backup mesh
            backup = alias + self.__BACKUP_SUFFIX__
            if cmds.objExists(backup):
                cmds.delete(backup)

    def move_from_bounding_box(self, obj, scale=False, factor=1.5):
        """
        Temporary, will offset mesh position from the neutral mesh,
        Needs to be redone with proper bounding box handling.

        :param float factor: offset factor to use
        """
        if not self.blend_shape_node:
            return

        # Bounding box reference
        references = self.blend_shape_node.deformed_objects()

        # Init grid handler
        grid = scene.Grid(references or obj, factor=factor)

        # Scale object
        if scale:
            grid.set_scale(obj)

        # Set object position
        grid.set_position(obj)

        return grid

    @staticmethod
    def assign_default_shader(mesh):
        """
        Assign custom shader to specified mesh

        :param str mesh: mesh name
        """
        shd = TargetShader()
        shd.assign_to(mesh)

    def rename_target(self, target, new_name, batch=True):
        """
        Rename the specified target

        :param str target: target name
        :param str new_name: new target
        :param bool batch: used to disable the name check and speedup processing in batch mode by avoiding double check.
        """
        if not batch:
            assert not new_name in self.blend_shape_node.aliases(), 'Alias %s is already used' % new_name

        # Rename alias
        name = self.blend_shape_node.rename_target(target, new_name)
        if name == target:
            return

        # Rename existing mesh
        for mesh in cmds.ls(target):
            api.Node(mesh).name = new_name

        # Rename existing backup mesh
        for mesh in cmds.ls(target + self.__BACKUP_SUFFIX__):
            api.Node(mesh).name = new_name + self.__BACKUP_SUFFIX__

    def remove_target(self, target_index, weight=1.0, geom_index=0):
        self.blend_shape_node.remove_target(target_index,
                                            weight=weight,
                                            geom_index=geom_index)

    def search_replace_targets(self, pattern, replace):
        """
        will search and replace all target names

        :param str pattern: search string or regular expression
        :param str replace: string to replace with
        :return type: list
        """
        # Reset progress bar
        if self.progress_bar:
            self.progress_bar.maximum = self.target_count()
            self.progress_bar.show()

        aliases = self.blend_shape_node.aliases() or list()
        for i in range(len(aliases)):
            # Update progress bar
            if self.progress_bar:
                self.progress_bar.value += 1

            # Define new alias
            new_alias = re.sub(pattern, replace, aliases[i])
            if new_alias == aliases[i]:
                continue

            # Skip already used aliases
            if new_alias in aliases:
                sys.stdout.write('# Failed to rename target "{}" to "{}", name already used\n'.format(aliases[i],
                                                                                                      new_alias))
                continue

            # Rename target
            self.rename_target(aliases[i], new_alias, batch=True)

            # Update list
            aliases[i] = new_alias

        return aliases

    def add_target(self, name, geom_index=0):
        """
        Create a copy of the neutral shape with the new target name and adds it to the blend shape

        :param str name: name of the new target
        :param int geom_index: geometry index
        """
        if name in self.blend_shape_node.aliases():
            return

        # Generate neutral shape
        neutral = self.generate_neutral_shape(name)

        # Apply target to blendShape
        self.apply_target(neutral,
                          target_index=self.blend_shape_node.next_index(),
                          geom_index=geom_index)

        return neutral

    @staticmethod
    def project_scenes_folder():
        """
        Return the current project folder path

        :return type: str
        """
        path = cmds.workspace(q=True, rootDirectory=True)
        return os.path.join(path, 'scenes')

    @staticmethod
    def mesh_input_blend_shape(mesh):
        """
        Return any ancestor blend shape node for mesh

        :param mesh: mesh to test
        :return type: list
        """
        return cmds.ls(cmds.listHistory(mesh, allGraphs=True), type="blendShape")

    @staticmethod
    def alias_combination(alias):
        """
        Will return the list of aliased composing the specified alias

        :param str alias: target combination name
        :return: list
        """
        # Init regular expression
        regex = re.compile(r'([a-zA-Z]+(?:_[C|L|R])*(?:_[0-9]+)*)')

        # Find valid name blocks contained in name
        components = regex.findall(alias)

        # Remove self from list
        if alias in components:
            components.remove(alias)

        components.sort()
        return components

    def _replace_target_index(self, mesh, alias, target_index, geom_index=0):
        """
        Replace the specified index with the mesh, will return the replaced target mesh

        :param str mesh: target mesh to apply
        :param str alias: name of the target
        :param int target_index: target index to replace
        :param int geom_index: geometry index
        :return:
        """
        # Get replaced target mesh
        old_mesh = None
        if target_index in self.blend_shape_node.indexes:
            old_mesh = self.blend_shape_node.generate_target_shape_by_index(target_index)
            self.remove_target(target_index)

        # Apply new data
        self.apply_target(mesh,
                          target_index=target_index,
                          weight=1.0,
                          geom_index=geom_index)

        # Return replaced data
        return old_mesh

    def reorder_targets(self):
        """
        Will reorder alphabetically the targets on the blendShape (intensive process)
        """
        # Filter aliases by combination level
        filtered_targets = dict()
        for alias in self.blend_shape_node.aliases():
            # Check combination content
            combinations = self.alias_combination(alias)
            if not combinations:
                if not 1 in filtered_targets:
                    filtered_targets[1] = [alias]
                else:
                    filtered_targets[1].append(alias)
            else:
                if not len(combinations) in filtered_targets:
                    filtered_targets[len(combinations)] = [alias]
                else:
                    filtered_targets[len(combinations)].append(alias)

        # Build ordered target list
        ordered_aliases = list()
        levels = filtered_targets.keys()
        levels.sort()
        for level in levels:
            ordered_aliases.extend(filtered_targets[level])

        # Init progress bar
        if self.progress_bar:
            self.progress_bar.show()
            self.progress_bar.maximum = len(ordered_aliases)

        # Process targets
        processed = list()
        for i in range(len(ordered_aliases)):
            alias = ordered_aliases[i]

            # Skip processed target
            if alias in processed:
                continue
            processed.append(alias)

            # Get current index
            current_index = self.blend_shape_node.alias_index(alias)
            if current_index == i:
                continue

            # Increment progress bar
            if self.progress_bar:
                self.progress_bar.value += 1

            # Generate target mesh
            mesh = self.blend_shape_node.generate_target_shape_by_index(current_index)

            # Remove target from blendShape to avoid name issue)
            self.remove_target(current_index)

            # Replace target data
            replaced = self._replace_target_index(mesh, alias, i)
            cmds.delete(mesh)
            mesh = replaced

            if not replaced:
                continue

            while replaced:
                # Increment progress bar
                if self.progress_bar:
                    self.progress_bar.value += 1

                # Process
                replaced = self._replace_target_index(mesh, mesh.name, ordered_aliases.index(mesh.name))
                cmds.delete(mesh)
                mesh = replaced
