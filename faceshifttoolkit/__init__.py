# This file is part of the faceshift toolkit, developed by faceshift (www.faceshift.com).
#
# The faceshift toolkit is free software:
#     you can redistribute it and/or modify it under the terms of the GNU General Public License,
#     either version 3 of the License, or (at your option) any later version.
#
# The faceshift toolkit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the faceshift toolkit.
# If not, see <http://www.gnu.org/licenses/>.


__DOCUMENTATION_URL__ = 'http://doc.faceshift.com/toolkit/'
__version__ = 0.1
__author__ = 'faceshift / guillaume barlier'
__url__ = 'https://bitbucket.org/faceshift/faceshift-toolkit'


import sys
sys.stdout.write('# The faceshift tool-kit is licensed under the GNU General Public License v3 or later\n')